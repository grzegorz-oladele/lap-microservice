# lap-microservice



## Description
Lap-microservice is a microservice that handles the storage and management of data related to motorcycle rides on 
selected motorcycle tracks. The service retrieves rider and motorcycle data from other microservices. It allows 
filtering data by date and motorcycle capacity, brand, and model. The microservice functions as a REST service.

## Installation/Usage
The lap-microservice is part of a larger, complex architecture. To run it, please refer to the 
[docker-compose](https://gitlab.com/grzegorz-oladele/docker-compose) where detailed instructions on running the entire 
architecture are provided.

## Example Usage
Below are example endpoints that can be used to interact with the application:

### LAPS
- [ ] <span style="color:green">GET</span> /laps?page=0&size=10 - return laps
- [ ] <span style="color:green">GET</span> /laps/{lapId}/bikers/{bikerId} - return lap by lap id 
- [ ] <span style="color:orange">POST</span> /laps/{circuitId}/circuits/{bikerId}/bikers/{motorcycleId}/motorcycles - 
create new lap

### CIRCUITS
- [ ] <span style="color:green">GET</span> /circuits?page=0&size=10 - return circuits
- [ ] <span style="color:green">GET</span> /circuits/{circuitId} - return circuit by circuit id
- [ ] <span style="color:orange">POST</span> /circuits/ - create new circuit
- [ ] <span style="color:purple">PATCH</span> /circuits/{circuitId} - update circuit by circuit id

## Architecture
The lap-microservice project is implemented using Domain-Driven Design (DDD) and event storming patterns. 
It consists of four modules:
- [ ] **server** - responsible for application startup, integration tests, and managing environment variables.
- [ ] **domain** - the core of the application, containing key objects and dedicated methods for working with those 
objects.
- [ ] **application** - exposes application use cases.
- [ ] **adapters** - facilitates communication between the application and external systems, as well as between other 
microservices in the cluster.

## Custom Pipeline
This repository includes a custom pipeline that automates the build, testing, and deployment process for the application
The pipeline is designed to perform the following steps:

1. Build and test:
    - [ ] The pipeline triggers a build process using Maven to compile and package the application.
    - [ ] It runs the tests to ensure the code quality and functionality.
    - [ ] If the build and tests pass successfully, the pipeline proceeds to the next step.
    - [ ] In case the testing process fails, the pipeline terminates, and the test logs are saved in the newly created
      **errors.txt** file

2. Commit and push:
    - [ ] The pipeline commits and pushes the changes to the Git repository on GitLab.
    - [ ] It includes the commit message provided as an argument when running the pipeline.

3. Docker image build and push:
    - [ ] The pipeline builds a Docker image of the application.
    - [ ] It tags the Docker image with the first 6 characters of the current commit's hash.
    - [ ] If the optional -d (docker) argument is present when running the pipeline, it automatically pushes the Docker
    image to the Docker Hub repository.

***Example command execution:*** `lap-build -cm"commit message" -d`

Before each pipeline execution, the script checks the existence of the pipeline folder and removes it if it already
exists. This ensures a clean environment for the pipeline to run successfully.

Please note that the pipeline assumes the necessary dependencies, such as Maven, Git, and Docker, are properly
installed and configured on the build environment.

## Project status
The lap-microservice project is actively being actively developed. We are constantly working on adding new features including:
- [ ] implementation of the CQRS pattern
- [ ] addition and implementation of Liquibase library
- [ ] implementation of security using the Spring Security framework
- [ ] increasing test coverage of key application functionalities

# Author
**Grzegorz Oladele**

Thank you for your interest in the lap-microservice project! If you have any questions or feedback, please 
feel free to contact us.
