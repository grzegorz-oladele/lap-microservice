FROM amazoncorretto:17-alpine
EXPOSE 8300
WORKDIR /app
COPY lap-service-erver/src/main/resources/db /app
COPY lap-service-erver/target/lap-service-server-0.0.1-SNAPSHOT.jar /app/lap-service-server-0.0.1-SNAPSHOT.jar
CMD ["java", "-jar", "lap-service-server-0.0.1-SNAPSHOT.jar"]