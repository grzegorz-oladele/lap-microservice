package pl.grzegorz.lapserviceserver;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.grzegorz.lapadapters.in.rest.dto.LapCreateDto;
import pl.grzegorz.lapadapters.out.persistence.biker.command.BikerEntity;
import pl.grzegorz.lapadapters.out.persistence.circuit.command.CircuitEntity;
import pl.grzegorz.lapadapters.out.persistence.lap.command.LapEntity;
import pl.grzegorz.lapadapters.out.persistence.motorcycle.command.MotorcycleEntity;

import java.time.LocalDate;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Fixtures {

    public static final UUID BIKER_ID = UUID.fromString("a12b492e-1641-4cc4-a8c7-e2c4bab6c8ba");
    public static final UUID UNACTIVE_BIKER_ID = UUID.fromString("e5226765-35c5-451d-a706-80e0ce5c4b20");
    public static final UUID MOTORCYCLE_ID = UUID.fromString("fd2a460c-9772-4d9c-9fe5-2e41f5a51815");
    public static final UUID UNACTIVE_MOTORCYCLE_ID = UUID.fromString("a86796a5-d66a-4e6f-a81c-e720fa041fd7");
    public static final UUID CIRCUIT_ID = UUID.fromString("d411de0d-a9f2-4844-bd39-437e1a9733e5");
    public static final UUID SECOND_BIKER_ID = UUID.fromString("d76e2c73-647b-43fd-bbe5-3938d0692c6d");

    public static LapCreateDto lapCreateDto() {
        return new LapCreateDto(
                "1:22.333",
                LocalDate.now().toString()
        );
    }

    public static LapCreateDto invalidLapCreateDto() {
        return new LapCreateDto(
                "1.22:333",
                LocalDate.now().toString()
        );
    }

    public static LapEntity lapEntity() {
        return LapEntity.builder()
                .withId(UUID.randomUUID())
                .withLapTime("1:22.333")
                .withLapDate(LocalDate.now())
                .withCircuitId(CIRCUIT_ID)
                .withBikerId(BIKER_ID)
                .withMotorcycleId(MOTORCYCLE_ID)
                .build();
    }

    public static CircuitEntity circuitEntity() {
        return CircuitEntity.builder()
                .withId(CIRCUIT_ID)
                .withName("Tor Poznań")
                .withDescription("The biggest circuit in Poland")
                .withStreetName("Wyścigowa")
                .withStreetNumber("3")
                .withCity("Poznań")
                .withEmail("tor@poznan.pl")
                .withPhoneNumber("123-456-789")
                .withLength(4.093)
                .withPostalCode("62-081")
                .build();
    }

    public static MotorcycleEntity motorcycleEntity() {
        return MotorcycleEntity.builder()
                .withId(MOTORCYCLE_ID)
                .withBikerId(BIKER_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withHorsePower(225)
                .withVintage(2021)
                .withCapacity(1199)
                .withIsActive(Boolean.TRUE)
                .withSerialNumber("IUBEVRBIUVE")
                .build();
    }

    public static MotorcycleEntity unactiveMotorcycleEntity() {
        return MotorcycleEntity.builder()
                .withId(UNACTIVE_MOTORCYCLE_ID)
                .withBikerId(BIKER_ID)
                .withBrand("Kawasaki")
                .withModel("ZX6R Ninja")
                .withHorsePower(123)
                .withVintage(2009)
                .withCapacity(599)
                .withIsActive(Boolean.FALSE)
                .withSerialNumber("NUIOVERNOIKCE")
                .build();
    }

    public static BikerEntity bikerEntity() {
        return BikerEntity.builder()
                .withId(BIKER_ID)
                .withUserName("tomasz_123")
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withEmail("tomasz@123.pl")
                .withPhoneNumber("123-456-789")
                .withEnable(Boolean.TRUE)
                .withDateOfBirth(LocalDate.of(1992, 4, 25))
                .build();
    }

    public static BikerEntity unactiveBikerEntity() {
        return BikerEntity.builder()
                .withId(UNACTIVE_BIKER_ID)
                .withUserName("Mariusz_123")
                .withFirstName("Mariusz")
                .withLastName("mariuszewski")
                .withEmail("mariusz@123.pl")
                .withPhoneNumber("234-335-478")
                .withEnable(Boolean.FALSE)
                .withDateOfBirth(LocalDate.of(1993, 8, 12))
                .build();
    }
}