package pl.grzegorz.lapserviceserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.util.Set;

@ActiveProfiles("test")
@Testcontainers
@AutoConfigureMockMvc
@SpringBootTest
public abstract class AbstractIntegrationTest {

    private static final Network INTEGRATION_TEST_NETWORK = Network.newNetwork();
    private static final String POSTGRES_IMAGE = "postgres:14-alpine";
    private static final String RABBITMQ_IMAGE = "rabbitmq:3.12.0-rc.3-management-alpine";
    private static final String LAP_TEST_DB = "lap_db";
    private static final String LAP_TEST_DB_USER = "lap";
    private static final String LAP_TEST_DB_PASSWORD = "lap";
    private static final String LAP_DB_ALIAS = "lap-db";
    private static final String BIKER_DISABLE_QUEUE = "bikers_lap_disable";
    private static final String BIKER_ENABLE_QUEUE = "bikers_lap_enable";
    private static final String BIKER_CREATE_QUEUE = "bikers_lap_persist";
    private static final String BIKER_UPDATE_QUEUE = "bikers_lap_update";
    private static final String MOTORCYCLE_CREATE_QUEUE = "motorcycles_lap_persist";
    private static final String MOTORCYCLE_UPDATE_QUEUE = "motorcycles_lap_update";
    private static final String MOTORCYCLE_UNACTIVE_QUEUE = "motorcycles_lap_unactive";
    private static final String CIRCUIT_CREATE_QUEUE = "circuit_lap_persist";
    private static final String CIRCUIT_UPDATE_QUEUE = "motorcycles_lap_update";
    private static final String RABBITMQ_DEFAULT_USER_TAG = "management";
    private static final String RABBIT_USER = "rabbit";
    private static final String RABBIT_PASSWORD = "rabbit";
    private static final String RABBITMQ_ALIAS = "rabbitmq";
    private static final int RABBITMQ_DEFAULT_PORT = 5672;

    private static final PostgreSQLContainer<?> LAP_SERVICE_POSTGRE_SQL_CONTAINER = new PostgreSQLContainer<>(
            DockerImageName.parse(POSTGRES_IMAGE))
            .withDatabaseName(LAP_TEST_DB)
            .withPassword(LAP_TEST_DB_PASSWORD)
            .withUsername(LAP_TEST_DB_USER)
            .withNetwork(INTEGRATION_TEST_NETWORK)
            .withNetworkAliases(LAP_DB_ALIAS);
    private static final RabbitMQContainer RABBIT_MQ_CONTAINER = new RabbitMQContainer(
            DockerImageName.parse(RABBITMQ_IMAGE))
            .withNetwork(INTEGRATION_TEST_NETWORK)
            .withNetworkAliases(RABBITMQ_ALIAS)
            .withExposedPorts(RABBITMQ_DEFAULT_PORT)
            .withUser(RABBIT_USER, RABBIT_PASSWORD, Set.of(RABBITMQ_DEFAULT_USER_TAG))
            .withQueue(BIKER_DISABLE_QUEUE)
            .withQueue(BIKER_ENABLE_QUEUE)
            .withQueue(BIKER_CREATE_QUEUE)
            .withQueue(BIKER_UPDATE_QUEUE)
            .withQueue(MOTORCYCLE_CREATE_QUEUE)
            .withQueue(MOTORCYCLE_UPDATE_QUEUE)
            .withQueue(MOTORCYCLE_UNACTIVE_QUEUE)
            .withQueue(CIRCUIT_UPDATE_QUEUE)
            .withQueue(CIRCUIT_CREATE_QUEUE);

    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected ObjectMapper mapper;

    static {
        LAP_SERVICE_POSTGRE_SQL_CONTAINER.start();
        RABBIT_MQ_CONTAINER.start();
        System.setProperty("spring.rabbitmq.port", RABBIT_MQ_CONTAINER.getMappedPort(RABBITMQ_DEFAULT_PORT).toString());
    }

    @DynamicPropertySource
    public static void postgresContainerConfig(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", LAP_SERVICE_POSTGRE_SQL_CONTAINER::getJdbcUrl);
        registry.add("spring.datasource.password", LAP_SERVICE_POSTGRE_SQL_CONTAINER::getPassword);
        registry.add("spring.datasource.username", LAP_SERVICE_POSTGRE_SQL_CONTAINER::getUsername);
    }
}