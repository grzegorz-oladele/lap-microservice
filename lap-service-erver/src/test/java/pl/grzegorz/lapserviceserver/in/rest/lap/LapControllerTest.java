package pl.grzegorz.lapserviceserver.in.rest.lap;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import pl.grzegorz.lapadapters.in.rest.dto.LapCreateDto;
import pl.grzegorz.lapadapters.out.persistence.biker.command.BikerEntity;
import pl.grzegorz.lapadapters.out.persistence.biker.command.BikerRepository;
import pl.grzegorz.lapadapters.out.persistence.circuit.command.CircuitEntity;
import pl.grzegorz.lapadapters.out.persistence.circuit.command.CircuitRepository;
import pl.grzegorz.lapadapters.out.persistence.lap.command.LapEntity;
import pl.grzegorz.lapadapters.out.persistence.lap.command.LapRepository;
import pl.grzegorz.lapadapters.out.persistence.motorcycle.command.MotorcycleEntity;
import pl.grzegorz.lapadapters.out.persistence.motorcycle.command.MotorcycleRepository;
import pl.grzegorz.lapserviceserver.AbstractIntegrationTest;

import java.time.LocalDate;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.grzegorz.lapserviceserver.Fixtures.*;

class LapControllerTest extends AbstractIntegrationTest {

    private static final String LAP_PATH = "/laps";

    @Autowired
    private LapRepository lapRepository;
    @Autowired
    private CircuitRepository circuitRepository;
    @Autowired
    private MotorcycleRepository motorcycleRepository;
    @Autowired
    private BikerRepository bikerRepository;
    private LapCreateDto lapCreateDto;
    private LapCreateDto invalidLapCreateDto;
    private LapEntity lapEntity;
    private CircuitEntity circuitEntity;
    private MotorcycleEntity motorcycleEntity;
    private MotorcycleEntity unactiveMotorcycleEntity;
    private BikerEntity bikerEntity;
    private BikerEntity unactiveBikerEntity;

    @BeforeEach
    void setup() {
        lapCreateDto = lapCreateDto();
        lapEntity = lapRepository.save(lapEntity());
        invalidLapCreateDto = invalidLapCreateDto();
        circuitEntity = circuitRepository.save(circuitEntity());
        motorcycleEntity = motorcycleRepository.save(motorcycleEntity());
        unactiveMotorcycleEntity = motorcycleRepository.save(unactiveMotorcycleEntity());
        bikerEntity = bikerRepository.save(bikerEntity());
        unactiveBikerEntity = bikerRepository.save(unactiveBikerEntity());
    }

    @AfterEach
    void teardown() {
        lapRepository.deleteAll();
        bikerRepository.deleteAll();
        circuitRepository.deleteAll();
        motorcycleRepository.deleteAll();
    }

    @Test
    void shouldCreateNewLap() throws Exception {
        var circuitId = lapEntity.circuitId();
        var bikerId = lapEntity.bikerId();
        var motorcycleId = lapEntity.motorcycleId();
        var mockDto = mapper.writeValueAsString(lapCreateDto);
        var result = mockMvc.perform(post(String.format(LAP_PATH + "/%s/circuits/%s/bikers/%s/motorcycles", circuitId, bikerId,
                        motorcycleId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mockDto))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString();
        var lap = lapRepository.findById(UUID.fromString(result.replace("\"", "")))
                .orElse(null);
        assertAll(
                () -> assertThat(lap.lapTime(), is("1:22.333")),
                () -> assertThat(lap.lapDate(), is(LocalDate.now())),
                () -> assertThat(lap.circuitId(), is(circuitEntity.id())),
                () -> assertThat(lap.bikerId(), is(bikerEntity.id())),
                () -> assertThat(lap.motorcycleId(), is(motorcycleEntity.id()))
        );
    }

    @Test
    void shouldThrowMotorcycleUnactiveExceptionWhenMotorcycleWillBeUnactive() throws Exception {
        var circuitId = lapEntity.circuitId();
        var bikerId = lapEntity.bikerId();
        var motorcycleId = unactiveMotorcycleEntity.id();
        var mockDto = mapper.writeValueAsString(lapCreateDto);
        mockMvc.perform(post(String.format(LAP_PATH + "/%s/circuits/%s/bikers/%s/motorcycles", circuitId, bikerId,
                        motorcycleId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mockDto))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    void shouldThrowBikerUnactiveExceptionWhenBikerWillBeUnactive() throws Exception {
        var circuitId = lapEntity.circuitId();
        var bikerId = unactiveBikerEntity.id();
        var motorcycleId = lapEntity.motorcycleId();
        var mockDto = mapper.writeValueAsString(lapCreateDto);
        mockMvc.perform(post(String.format(LAP_PATH + "/%s/circuits/%s/bikers/%s/motorcycles", circuitId, bikerId,
                        motorcycleId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mockDto))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.message", is(String.format("Biker using id -> [%s] is unactive",
                        bikerId))))
                .andExpect(jsonPath("$.responseCode", is(422)));
    }

    @Test
    void shouldThrowInvalidLapTimeFormatExceptionWhenLapTimeValueWillBeInvalid() throws Exception {
        var circuitId = lapEntity.circuitId();
        var bikerId = lapEntity.bikerId();
        var motorcycleId = lapEntity.motorcycleId();
        var mockDto = mapper.writeValueAsString(invalidLapCreateDto);
        mockMvc.perform(post(String.format(LAP_PATH + "/%s/circuits/%s/bikers/%s/motorcycles", circuitId, bikerId,
                        motorcycleId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mockDto))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("Invalid lap time format")))
                .andExpect(jsonPath("$.responseCode", is(400)));
    }

    @Test
    void shouldThrowCircuitNotFoundExceptionWhenCircuitWillBeNotExistsInDatabase() throws Exception {
        var bikerId = BIKER_ID.toString();
        var motorcycleId = MOTORCYCLE_ID.toString();
        var circuitId = UUID.randomUUID();
        var mockDto = mapper.writeValueAsString(lapCreateDto);
        mockMvc.perform(post(String.format(LAP_PATH + "/%s/circuits/%s/bikers/%s/motorcycles", circuitId, bikerId,
                        motorcycleId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mockDto))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message", is(String.format("Circuit using id -> [%s] not found", circuitId))))
                .andExpect(jsonPath("$.responseCode", is(404)));
    }

    @Test
    void shouldThrowMotorcycleNotFoundExceptionWhenMotorcycleWillBeNotExistsInDatabase() throws Exception {
        var bikerId = lapEntity.bikerId();
        var motorcycleId = UUID.randomUUID();
        var circuitId = CIRCUIT_ID.toString();
        var mockDto = mapper.writeValueAsString(lapCreateDto);
        mockMvc.perform(post(String.format(LAP_PATH + "/%s/circuits/%s/bikers/%s/motorcycles", circuitId, bikerId,
                        motorcycleId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mockDto))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message", is(String.format("Motorcycle using id -> [%s] not found", motorcycleId))))
                .andExpect(jsonPath("$.responseCode", is(404)));
    }

    @Test
    void shouldThrowBikerNotFoundExceptionWhenBikerWillBeNotExistsInDatabase() throws Exception {
        var bikerId = UUID.randomUUID();
        var circuitId = CIRCUIT_ID.toString();
        var motorcycleId = MOTORCYCLE_ID.toString();
        var mockDto = mapper.writeValueAsString(lapCreateDto);
        mockMvc.perform(post(String.format(LAP_PATH + "/%s/circuits/%s/bikers/%s/motorcycles", circuitId, bikerId,
                        motorcycleId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mockDto))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message", is(String.format("Biker using id -> [%s] not found", bikerId))))
                .andExpect(jsonPath("$.responseCode", is(404)));
    }

    @Test
    void shouldReturnLapById() throws Exception {
//        WHEN BIKER EXISTS
        var lapId = lapEntity.id();
        var bikerId = lapEntity.bikerId();
        var motorcycleId = lapEntity.motorcycleId();
        mockMvc.perform(get(String.format(LAP_PATH + "/%s/bikers/%s", lapId, bikerId)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.lapTime", is("1:22.333")))
                .andExpect(jsonPath("$.lapDate", is(LocalDate.now().toString())))
                .andExpect(jsonPath("$.biker.firstName", is("Tomasz")))
                .andExpect(jsonPath("$.biker.email", is("tomasz@123.pl")))
                .andExpect(jsonPath("$.circuit.name", is("Tor Poznań")))
                .andExpect(jsonPath("$.circuit.city", is("Poznań")))
                .andExpect(jsonPath("$.circuit.length", is(4.093)))
                .andExpect(jsonPath("$.motorcycle.brand", is("Ducati")))
                .andExpect(jsonPath("$.motorcycle.model", is("Panigale V4S")))
                .andExpect(jsonPath("$.motorcycle.capacity", is(1199)))
                .andExpect(jsonPath("$.motorcycle.horsePower", is(225)))
                .andExpect(jsonPath("$.motorcycle.vintage", is(2021)));
    }

    @Test
    void shouldThrowBikerNotFoundExceptionWhenBikerWillBeNotExists() throws Exception {
        var bikerId = UUID.randomUUID();
        var lapId = lapEntity.id();
        mockMvc.perform(get(String.format(LAP_PATH + "/%s/bikers/%s", lapId, bikerId)))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message", is(String.format("Biker using id -> [%s] not found", bikerId))))
                .andExpect(jsonPath("$.responseCode", is(404)));
    }

    @Test
    void shouldReturnPageWithListOfLaps() throws Exception {
        var bikerId = lapEntity.bikerId();
        var motorcycleId = lapEntity.motorcycleId();
        mockMvc.perform(get(LAP_PATH))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pageInfo.pageSize", is(10)))
                .andExpect(jsonPath("$.pageInfo.actualPage", is(0)))
                .andExpect(jsonPath("$.pageInfo.totalPages", is(1)))
                .andExpect(jsonPath("$.pageInfo.totalRecordCount", is(1)))
                .andExpect(jsonPath("$.results[0].lapTime", is("1:22.333")))
                .andExpect(jsonPath("$.results[0].lapDate", is(LocalDate.now().toString())))
                .andExpect(jsonPath("$.results[0].biker.firstName", is("Tomasz")))
                .andExpect(jsonPath("$.results[0].biker.email", is("tomasz@123.pl")))
                .andExpect(jsonPath("$.results[0].circuit.name", is("Tor Poznań")))
                .andExpect(jsonPath("$.results[0].circuit.city", is("Poznań")))
                .andExpect(jsonPath("$.results[0].circuit.length", is(4.093)))
                .andExpect(jsonPath("$.results[0].motorcycle.brand", is("Ducati")))
                .andExpect(jsonPath("$.results[0].motorcycle.model", is("Panigale V4S")))
                .andExpect(jsonPath("$.results[0].motorcycle.capacity", is(1199)))
                .andExpect(jsonPath("$.results[0].motorcycle.horsePower", is(225)))
                .andExpect(jsonPath("$.results[0].motorcycle.vintage", is(2021)));
    }
}