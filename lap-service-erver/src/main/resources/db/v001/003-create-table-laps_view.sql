CREATE OR REPLACE VIEW lap_db.laps.laps_view AS
SELECT id,
       lap_time,
       lap_date,
       circuit_id,
       motorcycle_id,
       biker_id,
       created_at,
       modified_at
FROM lap_db.laps.laps;