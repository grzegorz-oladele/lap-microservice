CREATE OR REPLACE VIEW lap_db.laps.circuits_view AS
SELECT id,
       name,
       description,
       city,
       postal_code,
       street_name,
       street_number,
       length,
       email,
       phone_number,
       created_at,
       modified_at
from lap_db.laps.circuits;