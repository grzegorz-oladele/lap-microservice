CREATE TABLE lap_db.laps.circuits
(
    id            uuid primary key not null unique,
    name          varchar(56),
    description   varchar(400),
    city          varchar(56),
    postal_code   varchar(6),
    street_name   varchar(56),
    street_number varchar(12),
    length        double precision,
    email         varchar(56),
    phone_number  varchar(13),
    created_at    timestamp,
    modified_at   timestamp
)