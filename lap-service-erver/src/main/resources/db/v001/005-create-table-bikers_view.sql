CREATE OR REPLACE VIEW lap_db.laps.bikers_view AS
SELECT id,
       first_name,
       last_name,
       user_name,
       email,
       phone_number,
       date_of_birth,
       created_at,
       modified_at,
       enable
FROM lap_db.laps.bikers;