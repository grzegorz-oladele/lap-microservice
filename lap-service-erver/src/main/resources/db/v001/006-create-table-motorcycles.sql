CREATE TABLE lap_db.laps.motorcycles
(
    id               uuid primary key unique not null,
    biker_id         uuid                    not null,
    brand            varchar(56),
    model            varchar(56),
    capacity         integer,
    horse_power      integer,
    vintage          integer,
    serial_number    varchar(25),
    motorcycle_class varchar(20),
    created_at       timestamp,
    modified_at      timestamp,
    is_active        boolean
)