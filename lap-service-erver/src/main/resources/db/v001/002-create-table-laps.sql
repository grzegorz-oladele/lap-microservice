CREATE TABLE lap_db.laps.laps
(
    id            uuid primary key not null unique,
    lap_time      varchar(8),
    lap_date      date,
    circuit_id    uuid,
    motorcycle_id uuid,
    biker_id      uuid,
    created_at    timestamp,
    modified_at   timestamp
);