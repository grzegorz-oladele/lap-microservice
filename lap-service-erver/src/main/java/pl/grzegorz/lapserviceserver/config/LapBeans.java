package pl.grzegorz.lapserviceserver.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.grzegorz.lapapplication.ports.in.biker.command.BikerCreateCommandUseCase;
import pl.grzegorz.lapapplication.ports.in.biker.command.BikerDisableCommandUseCase;
import pl.grzegorz.lapapplication.ports.in.biker.command.BikerEnableCommandUseCase;
import pl.grzegorz.lapapplication.ports.in.biker.command.BikerUpdateCommandUseCase;
import pl.grzegorz.lapapplication.ports.in.circuit.command.CircuitCreateCommandUseCase;
import pl.grzegorz.lapapplication.ports.in.circuit.command.CircuitUpdateCommandUseCase;
import pl.grzegorz.lapapplication.ports.in.lap.command.LapCreateCommandUseCase;
import pl.grzegorz.lapapplication.ports.in.lap.query.LapByIdQueryUseCase;
import pl.grzegorz.lapapplication.ports.in.lap.query.LapPageQueryUseCase;
import pl.grzegorz.lapapplication.ports.in.motorcycle.command.MotorcycleCreateCommandUseCase;
import pl.grzegorz.lapapplication.ports.in.motorcycle.command.MotorcycleMapEngineCommandUseCase;
import pl.grzegorz.lapapplication.ports.in.motorcycle.command.MotorcycleOwnerUpdateCommandUseCase;
import pl.grzegorz.lapapplication.ports.in.motorcycle.command.MotorcycleSetUnactiveCommandUseCase;
import pl.grzegorz.lapapplication.ports.out.biker.command.BikerCreateCommandPort;
import pl.grzegorz.lapapplication.ports.out.biker.command.BikerDisableCommandPort;
import pl.grzegorz.lapapplication.ports.out.biker.command.BikerEnableCommandPort;
import pl.grzegorz.lapapplication.ports.out.biker.command.BikerUpdateCommandPort;
import pl.grzegorz.lapapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.lapapplication.ports.out.circuit.command.CircuitCreateCommandPort;
import pl.grzegorz.lapapplication.ports.out.circuit.command.CircuitUpdateCommandPort;
import pl.grzegorz.lapapplication.ports.out.circuit.query.CircuitByIdQueryPort;
import pl.grzegorz.lapapplication.ports.out.lap.command.LapCreateCommandPort;
import pl.grzegorz.lapapplication.ports.out.lap.query.LapByIdQueryPort;
import pl.grzegorz.lapapplication.ports.out.lap.query.LapPageQueryPort;
import pl.grzegorz.lapapplication.ports.out.motorcycle.command.MotorcycleCreateCommandPort;
import pl.grzegorz.lapapplication.ports.out.motorcycle.command.MotorcycleMapEngineCommandPort;
import pl.grzegorz.lapapplication.ports.out.motorcycle.command.MotorcycleOwnerUpdateCommandPort;
import pl.grzegorz.lapapplication.ports.out.motorcycle.command.MotorcycleSetUnactiveCommandPort;
import pl.grzegorz.lapapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.lapapplication.services.biker.BikerCreateCommandService;
import pl.grzegorz.lapapplication.services.biker.BikerDisableCommandService;
import pl.grzegorz.lapapplication.services.biker.BikerEnableCommandService;
import pl.grzegorz.lapapplication.services.biker.BikerUpdateCommandService;
import pl.grzegorz.lapapplication.services.circuit.command.CircuitCreateCommandService;
import pl.grzegorz.lapapplication.services.circuit.command.CircuitUpdateCommandService;
import pl.grzegorz.lapapplication.services.lap.command.LapCreateCommandService;
import pl.grzegorz.lapapplication.services.lap.query.LapByIdQueryService;
import pl.grzegorz.lapapplication.services.lap.query.LapPageQueryService;
import pl.grzegorz.lapapplication.services.motorcycle.commad.MotorcycleCreateCommandService;
import pl.grzegorz.lapapplication.services.motorcycle.commad.MotorcycleMapEngineCommandService;
import pl.grzegorz.lapapplication.services.motorcycle.commad.MotorcycleOwnerUpdateCommandService;
import pl.grzegorz.lapapplication.services.motorcycle.commad.MotorcycleSetUnactiveCommandService;

@Configuration
class LapBeans {

    @Bean
    public Jackson2JsonMessageConverter jsonMessageConverter() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        return new Jackson2JsonMessageConverter(mapper);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public SimpleRabbitListenerContainerFactory simpleRabbitListenerContainerFactory() {
        return new SimpleRabbitListenerContainerFactory();
    }

    @Bean
    public LapByIdQueryUseCase lapByIdQueryUseCase(LapByIdQueryPort lapByIdQueryPort,
                                                   BikerByIdQueryPort bikerByIdQueryPort,
                                                   MotorcycleByIdQueryPort motorcycleByIdQueryPort,
                                                   CircuitByIdQueryPort circuitByIdQueryPort) {
        return new LapByIdQueryService(lapByIdQueryPort, bikerByIdQueryPort, motorcycleByIdQueryPort,
                circuitByIdQueryPort);
    }

    @Bean
    public LapPageQueryUseCase lapPageQueryUseCase(LapPageQueryPort lapPageQueryPort,
                                                   MotorcycleByIdQueryPort motorcycleByIdQueryPort,
                                                   BikerByIdQueryPort bikerByIdQueryPort,
                                                   CircuitByIdQueryPort circuitByIdQueryPort) {
        return new LapPageQueryService(lapPageQueryPort, motorcycleByIdQueryPort, bikerByIdQueryPort, circuitByIdQueryPort);
    }

    @Bean
    public LapCreateCommandUseCase lapCreateCommandUseCase(CircuitByIdQueryPort circuitByIdQueryPort,
                                                           BikerByIdQueryPort bikerByIdQueryPort,
                                                           MotorcycleByIdQueryPort motorcycleByIdQueryPort,
                                                           LapCreateCommandPort lapCreateCommandPort) {
        return new LapCreateCommandService(circuitByIdQueryPort, bikerByIdQueryPort, motorcycleByIdQueryPort,
                lapCreateCommandPort);
    }

    @Bean
    public BikerCreateCommandUseCase bikerCreateCommandUseCase(BikerCreateCommandPort bikerCreateCommandPort) {
        return new BikerCreateCommandService(bikerCreateCommandPort);
    }

    @Bean
    public BikerUpdateCommandUseCase bikerUpdateCommandUseCase(BikerUpdateCommandPort bikerUpdateCommandPort,
                                                               BikerByIdQueryPort bikerByIdQueryPort) {
        return new BikerUpdateCommandService(bikerUpdateCommandPort, bikerByIdQueryPort);
    }

    @Bean
    public BikerEnableCommandUseCase bikerEnableCommandUseCase(BikerByIdQueryPort bikerByIdQueryPort,
                                                               BikerEnableCommandPort bikerEnableCommandPort) {
        return new BikerEnableCommandService(bikerByIdQueryPort, bikerEnableCommandPort);
    }

    @Bean
    public BikerDisableCommandUseCase bikerDisableCommandPort(BikerByIdQueryPort bikerByIdQueryPort,
                                                              BikerDisableCommandPort bikerDisableCommandPort) {
        return new BikerDisableCommandService(bikerByIdQueryPort, bikerDisableCommandPort);
    }

    @Bean
    public CircuitCreateCommandUseCase circuitCreateCommandUseCase(CircuitCreateCommandPort circuitCreateCommandPort) {
        return new CircuitCreateCommandService(circuitCreateCommandPort);
    }

    @Bean
    public CircuitUpdateCommandUseCase circuitUpdateCommandUseCase(CircuitByIdQueryPort circuitByIdQueryPort,
                                                                   CircuitUpdateCommandPort circuitUpdateCommandPort) {
        return new CircuitUpdateCommandService(circuitByIdQueryPort, circuitUpdateCommandPort);
    }

    @Bean
    public MotorcycleCreateCommandUseCase motorcycleCreateCommandUseCase(MotorcycleCreateCommandPort
                                                                                 motorcycleCreateCommandPort) {
        return new MotorcycleCreateCommandService(motorcycleCreateCommandPort);
    }

    @Bean
    public MotorcycleSetUnactiveCommandUseCase motorcycleSetUnactiveCommandUseCase(MotorcycleByIdQueryPort
                                                                                           motorcycleByIdQueryPort,
                                                                                   MotorcycleSetUnactiveCommandPort
                                                                                           motorcycleSetUnactiveCommandPort) {
        return new MotorcycleSetUnactiveCommandService(motorcycleByIdQueryPort, motorcycleSetUnactiveCommandPort);
    }

    @Bean
    public MotorcycleMapEngineCommandUseCase motorcycleMapEngineCommandUseCase(MotorcycleByIdQueryPort motorcycleByIdQueryPort,
                                                                               MotorcycleMapEngineCommandPort motorcycleMapEngineCommandPort) {
        return new MotorcycleMapEngineCommandService(motorcycleByIdQueryPort, motorcycleMapEngineCommandPort);
    }

    @Bean
    public MotorcycleOwnerUpdateCommandUseCase motorcycleOwnerUpdateCommandUseCase(MotorcycleByIdQueryPort motorcycleByIdQueryPort,
                                                                                   MotorcycleOwnerUpdateCommandPort motorcycleOwnerUpdateCommandPort) {
        return new MotorcycleOwnerUpdateCommandService(motorcycleByIdQueryPort, motorcycleOwnerUpdateCommandPort);
    }
}