package pl.grzegorz.lapserviceserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"pl.grzegorz"})
@EnableJpaRepositories(basePackages = {"pl.grzegorz"})
@EntityScan(basePackages = {"pl.grzegorz"})
@ConfigurationPropertiesScan(basePackages = {"pl.grzegorz"})
@EnableFeignClients(basePackages = {"pl.grzegorz"})
public class LapServiceErverApplication {

    public static void main(String[] args) {
        SpringApplication.run(LapServiceErverApplication.class, args);
    }

}
