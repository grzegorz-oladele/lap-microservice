package pl.grzegorz.lapdomain.aggregates;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.grzegorz.lapdomain.data.lap.LapCreateData;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static pl.grzegorz.lapdomain.Fixtures.lapCreateData;

class LapAggregateTest {

    private LapCreateData lapCreateData;

    @BeforeEach
    void setup() {
        lapCreateData = lapCreateData();
    }

    @Test
    void shouldCreateNewLapAggregateObject() {
//        given
//        when
        var aggregate = LapAggregate.create(lapCreateData);
        assertAll(
                () -> assertThat(aggregate.lapTime(), is(lapCreateData.lapTime())),
                () -> assertThat(aggregate.lapDate(), is(lapCreateData.lapDate()))
        );
    }
}