package pl.grzegorz.lapdomain.aggregates;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.grzegorz.lapdomain.data.circuit.CircuitCreateData;
import pl.grzegorz.lapdomain.data.circuit.CircuitUpdateData;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static pl.grzegorz.lapdomain.Fixtures.*;

class CircuitAggregateTest {

    private CircuitCreateData circuitCreateData;
    private CircuitUpdateData circuitUpdateData;

    @BeforeEach
    void setup() {
        circuitCreateData = circuitCreateData();
        circuitUpdateData = circuitUpdateData();
    }

    @Test
    void shouldCreateNewCircuitAggregateObject() {
//        given
//        when
        var aggregate = CircuitAggregate.create(circuitCreateData);
//        then
        assertAll(
                () -> assertThat(aggregate.id(), is(circuitCreateData.id())),
                () -> assertThat(aggregate.name(), is(circuitCreateData.name())),
                () -> assertThat(aggregate.description(), is(circuitCreateData.description())),
                () -> assertThat(aggregate.city(), is(circuitCreateData.city())),
                () -> assertThat(aggregate.postalCode(), is(circuitCreateData.postalCode())),
                () -> assertThat(aggregate.streetName(), is(circuitCreateData.streetName())),
                () -> assertThat(aggregate.streetNumber(), is(circuitCreateData.streetNumber())),
                () -> assertThat(aggregate.phoneNumber(), is(circuitCreateData.phoneNumber())),
                () -> assertThat(aggregate.email(), is(circuitCreateData.email())),
                () -> assertThat(aggregate.length(), is(circuitCreateData.length()))
        );
    }

    @Test
    void shouldUpdateCircuitAggregateObject() {
//        given
//        when
        var updatedAggregate = CircuitAggregate.update(circuitUpdateData);
//        then
        assertAll(
                () -> assertThat(updatedAggregate.id(), is(circuitUpdateData.id())),
                () -> assertThat(updatedAggregate.name(), is(circuitUpdateData.name())),
                () -> assertThat(updatedAggregate.description(), is(circuitUpdateData.description())),
                () -> assertThat(updatedAggregate.city(), is(circuitUpdateData.city())),
                () -> assertThat(updatedAggregate.postalCode(), is(circuitUpdateData.postalCode())),
                () -> assertThat(updatedAggregate.streetName(), is(circuitUpdateData.streetName())),
                () -> assertThat(updatedAggregate.streetNumber(), is(circuitUpdateData.streetNumber())),
                () -> assertThat(updatedAggregate.phoneNumber(), is(circuitUpdateData.phoneNumber())),
                () -> assertThat(updatedAggregate.email(), is(circuitUpdateData.email())),
                () -> assertThat(updatedAggregate.length(), is(circuitUpdateData.length()))
        );
    }
}