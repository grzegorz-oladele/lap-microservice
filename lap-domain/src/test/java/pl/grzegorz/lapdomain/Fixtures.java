package pl.grzegorz.lapdomain;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.lapdomain.data.MotorcycleMapEngineData;
import pl.grzegorz.lapdomain.data.MotorcycleOwnerUpdateData;
import pl.grzegorz.lapdomain.data.biker.BikerCreateData;
import pl.grzegorz.lapdomain.data.biker.BikerUpdateData;
import pl.grzegorz.lapdomain.data.circuit.CircuitCreateData;
import pl.grzegorz.lapdomain.data.circuit.CircuitUpdateData;
import pl.grzegorz.lapdomain.data.lap.LapCreateData;
import pl.grzegorz.lapdomain.data.motorcycle.MotorcycleClass;
import pl.grzegorz.lapdomain.data.motorcycle.MotorcycleCreateData;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Fixtures {

    private static final UUID BIKER_ID = UUID.fromString("c90fd34d-1ae5-4ae4-8ccd-2eadf7e01fcd");
    private static final UUID MOTORCYCLE_ID = UUID.fromString("6044a914-bf11-4cc4-bc81-6f74d5840db5");
    private static final UUID RECIPIENT_ID = UUID.fromString("3d445de6-3ada-4e44-9072-2b962d07d6f8");
    private static final UUID CIRCUIT_ID = UUID.fromString("edb42036-b5c0-4c03-b9ab-f5b75b82c6d9");

    public static BikerAggregate bikerAggregate() {
        return BikerAggregate.builder()
                .withId(BIKER_ID)
                .withUserName("biker_123")
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withEmail("tomasz@123.pl")
                .withIsActive(Boolean.TRUE)
                .withPhoneNumber("123-456-789")
                .withDateOfBirth(LocalDate.of(1989, 2, 23))
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }

    public static BikerAggregate disableBikerAggregate() {
        return BikerAggregate.builder()
                .withId(BIKER_ID)
                .withUserName("biker_123")
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withEmail("tomasz@123.pl")
                .withIsActive(Boolean.FALSE)
                .withPhoneNumber("123-456-789")
                .withDateOfBirth(LocalDate.of(1989, 2, 23))
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }

    public static BikerCreateData bikerCreateData() {
        return BikerCreateData.builder()
                .withId(BIKER_ID)
                .withUsername("biker_123")
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withEmail("tomasz@123.pl")
                .withEnable(Boolean.TRUE)
                .withPhoneNumber("123-456-789")
                .withDateOfBirth(LocalDate.of(1989, 2, 23).toString())
                .build();
    }

    public static BikerUpdateData bikerUpdateData() {
        return BikerUpdateData.builder()
                .withId(BIKER_ID)
                .withUsername("biker_123")
                .withFirstName("Patryk")
                .withLastName("Patrykowski")
                .withEmail("patryk@123.pl")
                .withEnable(Boolean.TRUE)
                .withPhoneNumber("465-039-375")
                .withDateOfBirth(LocalDate.of(1989, 2, 23))
                .build();
    }

    public static MotorcycleAggregate motorcycleAggregate() {
        return MotorcycleAggregate.builder()
                .withId(MOTORCYCLE_ID)
                .withBikerId(BIKER_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withCapacity(1199)
                .withHorsePower(225)
                .withVintage(2022)
                .withSerialNumber("IBUWFCE782BJKCWE")
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .withIsActive(Boolean.TRUE)
                .build();
    }

    public static MotorcycleCreateData motorcycleCreateData() {
        return MotorcycleCreateData.builder()
                .withId(MOTORCYCLE_ID)
                .withBikerId(BIKER_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withCapacity(1199)
                .withHorsePower(225)
                .withVintage(2022)
                .withSerialNumber("IBUWFCE782BJKCWE")
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT.toString())
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }

    public static MotorcycleMapEngineData motorcycleMapEngineData() {
        return MotorcycleMapEngineData.builder()
                .withHorsePower(235)
                .withCapacity(1300)
                .build();
    }

    public static MotorcycleOwnerUpdateData motorcycleOwnerUpdateData() {
        return new MotorcycleOwnerUpdateData(RECIPIENT_ID);
    }

    public static CircuitCreateData circuitCreateData() {
        return CircuitCreateData.builder()
                .withId(CIRCUIT_ID)
                .withName("To Poznań")
                .withDescription("The biggest circuit in Poland")
                .withCity("Poznań")
                .withPostalCode("62-081")
                .withStreetName("Wyścigowa")
                .withPhoneNumber("61-853-61-03")
                .withStreetNumber("3")
                .withLength(4.087)
                .withEmail("tor@poznan.pl")
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }

    public static CircuitUpdateData circuitUpdateData() {
        return CircuitUpdateData.builder()
                .withId(CIRCUIT_ID)
                .withName("To Poznań")
                .withDescription("The biggest circuit")
                .withCity("Poznań")
                .withPostalCode("66-081")
                .withStreetName("Wyścigowa")
                .withPhoneNumber("61-453-61-03")
                .withStreetNumber("3")
                .withLength(4.087)
                .withEmail("tor_poznan@poznan.pl")
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }

    public static LapCreateData lapCreateData() {
        return LapCreateData.builder()
                .withLapTime("1:33.938")
                .withLapDate(LocalDate.now())
                .withBikerId(BIKER_ID)
                .withMotorcycleId(MOTORCYCLE_ID)
                .withCircuitId(CIRCUIT_ID)
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }
}