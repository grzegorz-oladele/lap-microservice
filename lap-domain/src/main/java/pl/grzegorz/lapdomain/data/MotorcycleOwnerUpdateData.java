package pl.grzegorz.lapdomain.data;

import java.util.UUID;

public record MotorcycleOwnerUpdateData(
        UUID newMotorcycleOwnerId
) {
}