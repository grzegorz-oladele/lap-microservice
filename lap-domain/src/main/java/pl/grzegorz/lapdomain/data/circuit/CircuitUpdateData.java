package pl.grzegorz.lapdomain.data.circuit;

import lombok.Builder;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder(setterPrefix = "with")
public record CircuitUpdateData(
        UUID id,
        String name,
        String description,
        String city,
        String postalCode,
        String streetName,
        String streetNumber,
        double length,
        String email,
        String phoneNumber,
        LocalDateTime createdAt,
        LocalDateTime modifiedAt
) {
}