package pl.grzegorz.lapdomain.data;

import lombok.Builder;

import java.util.UUID;

@Builder(setterPrefix = "with")
public record MotorcycleMapEngineData(
        UUID id,
        UUID bikerId,
        int capacity,
        int horsePower
) {
}