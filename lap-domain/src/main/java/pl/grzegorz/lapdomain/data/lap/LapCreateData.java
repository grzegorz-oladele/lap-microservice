package pl.grzegorz.lapdomain.data.lap;

import lombok.Builder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Builder(setterPrefix = "with")
public record LapCreateData(
        String lapTime,
        LocalDate lapDate,
        UUID bikerId,
        UUID motorcycleId,
        UUID circuitId,
        LocalDateTime createdAt,
        LocalDateTime modifiedAt
) {
}