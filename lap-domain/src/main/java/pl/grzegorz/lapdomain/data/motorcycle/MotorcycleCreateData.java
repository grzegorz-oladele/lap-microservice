package pl.grzegorz.lapdomain.data.motorcycle;

import lombok.Builder;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder(setterPrefix = "with")
public record MotorcycleCreateData(
        UUID id,
        String brand,
        UUID bikerId,
        String model,
        int capacity,
        int horsePower,
        int vintage,
        String serialNumber,
        String motorcycleClass,
        LocalDateTime createdAt,
        LocalDateTime modifiedAt
) {
}