package pl.grzegorz.lapdomain.data.biker;

import lombok.Builder;

import java.util.UUID;

@Builder(setterPrefix = "with")
public record BikerDetails(
        UUID id,
        String firstName,
        String username,
        String email
) {
}