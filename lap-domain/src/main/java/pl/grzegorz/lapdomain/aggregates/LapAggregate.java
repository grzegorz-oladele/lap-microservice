package pl.grzegorz.lapdomain.aggregates;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;
import pl.grzegorz.lapdomain.data.lap.LapCreateData;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Accessors(fluent = true)
@Builder(toBuilder = true, setterPrefix = "with")
public class LapAggregate {

    private UUID id;
    private String lapTime;
    private LocalDate lapDate;
    private UUID circuitId;
    private UUID bikerId;
    private UUID motorcycleId;
    private LocalDateTime createdAt;
    private LocalDateTime modifiedAt;

    public static LapAggregate create(LapCreateData lapCreateData) {
        return LapAggregate.builder()
                .withId(UUID.randomUUID())
                .withLapDate(lapCreateData.lapDate())
                .withLapTime(lapCreateData.lapTime())
                .withCreatedAt(lapCreateData.createdAt())
                .withModifiedAt(lapCreateData.modifiedAt())
                .build();
    }
}