package pl.grzegorz.lapdomain.aggregates;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;
import pl.grzegorz.lapdomain.data.biker.BikerCreateData;
import pl.grzegorz.lapdomain.data.biker.BikerUpdateData;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Accessors(fluent = true)
@Builder(toBuilder = true, setterPrefix = "with")
public class BikerAggregate {

    private UUID id;
    private String firstName;
    private String lastName;
    private String userName;
    private String email;
    private Boolean isActive;
    private String phoneNumber;
    private LocalDate dateOfBirth;
    private LocalDateTime createdAt;
    private LocalDateTime modifiedAt;

    public static BikerAggregate create(BikerCreateData bikerCreateData) {
        return BikerAggregate.builder()
                .withId(bikerCreateData.id())
                .withFirstName(bikerCreateData.firstName())
                .withLastName(bikerCreateData.lastName())
                .withUserName(bikerCreateData.username())
                .withEmail(bikerCreateData.email())
                .withIsActive(bikerCreateData.enable())
                .withPhoneNumber(bikerCreateData.phoneNumber())
                .withDateOfBirth(LocalDate.parse(bikerCreateData.dateOfBirth()))
                .withCreatedAt(bikerCreateData.createdAt())
                .withModifiedAt(bikerCreateData.modifiedAt())
                .build();
    }

    public static BikerAggregate update(BikerUpdateData bikerUpdateData) {
        return BikerAggregate.builder()
                .withId(bikerUpdateData.id())
                .withFirstName(bikerUpdateData.firstName())
                .withLastName(bikerUpdateData.lastName())
                .withUserName(bikerUpdateData.username())
                .withEmail(bikerUpdateData.email())
                .withIsActive(bikerUpdateData.enable())
                .withPhoneNumber(bikerUpdateData.phoneNumber())
                .withDateOfBirth(bikerUpdateData.dateOfBirth())
                .withCreatedAt(bikerUpdateData.createdAt())
                .withModifiedAt(bikerUpdateData.modifiedAt())
                .build();
    }

    public void enable() {
        isActive = Boolean.TRUE;
        modifiedAt = LocalDateTime.now();
    }

    public void disable() {
        isActive = Boolean.FALSE;
        modifiedAt = LocalDateTime.now();
    }
}