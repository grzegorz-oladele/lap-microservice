package pl.grzegorz.lapdomain.aggregates;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;
import pl.grzegorz.lapdomain.data.circuit.CircuitCreateData;
import pl.grzegorz.lapdomain.data.circuit.CircuitUpdateData;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Accessors(fluent = true)
@Builder(toBuilder = true, setterPrefix = "with")
public class CircuitAggregate {

    private final UUID id;
    private final String name;
    private final String description;
    private final String city;
    private final String postalCode;
    private final String streetName;
    private final String streetNumber;
    private final double length;
    private final String email;
    private final String phoneNumber;
    private final LocalDateTime createdAt;
    private final LocalDateTime modifiedAt;

    public static CircuitAggregate create(CircuitCreateData circuitCreateData) {
        return CircuitAggregate.builder()
                .withId(circuitCreateData.id())
                .withName(circuitCreateData.name())
                .withDescription(circuitCreateData.description())
                .withCity(circuitCreateData.city())
                .withPostalCode(circuitCreateData.postalCode())
                .withStreetName(circuitCreateData.streetName())
                .withPhoneNumber(circuitCreateData.phoneNumber())
                .withStreetNumber(circuitCreateData.streetNumber())
                .withLength(circuitCreateData.length())
                .withEmail(circuitCreateData.email())
                .withCreatedAt(LocalDateTime.now())
                .withModifiedAt(LocalDateTime.now())
                .build();
    }

    public static CircuitAggregate update(CircuitUpdateData circuitUpdateData) {
        return CircuitAggregate.builder()
                .withId(circuitUpdateData.id())
                .withName(circuitUpdateData.name())
                .withDescription(circuitUpdateData.description())
                .withCity(circuitUpdateData.city())
                .withPostalCode(circuitUpdateData.postalCode())
                .withStreetName(circuitUpdateData.streetName())
                .withPhoneNumber(circuitUpdateData.phoneNumber())
                .withStreetNumber(circuitUpdateData.streetNumber())
                .withLength(circuitUpdateData.length())
                .withEmail(circuitUpdateData.email())
                .withCreatedAt(circuitUpdateData.createdAt())
                .withModifiedAt(circuitUpdateData.modifiedAt())
                .build();
    }
}