package pl.grzegorz.lapdomain.aggregates;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;
import pl.grzegorz.lapdomain.data.MotorcycleMapEngineData;
import pl.grzegorz.lapdomain.data.MotorcycleOwnerUpdateData;
import pl.grzegorz.lapdomain.data.motorcycle.MotorcycleClass;
import pl.grzegorz.lapdomain.data.motorcycle.MotorcycleCreateData;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Accessors(fluent = true)
@Builder(toBuilder = true, setterPrefix = "with")
public class MotorcycleAggregate {

    private UUID id;
    private UUID bikerId;
    private String brand;
    private String model;
    private Integer capacity;
    private Integer horsePower;
    private Integer vintage;
    private String serialNumber;
    private MotorcycleClass motorcycleClass;
    private LocalDateTime createdAt;
    private LocalDateTime modifiedAt;
    private Boolean isActive;

    public static MotorcycleAggregate create(MotorcycleCreateData createData) {
        return MotorcycleAggregate.builder()
                .withId(createData.id())
                .withBikerId(createData.bikerId())
                .withBrand(createData.brand())
                .withModel(createData.model())
                .withCapacity(createData.capacity())
                .withHorsePower(createData.horsePower())
                .withVintage(createData.vintage())
                .withSerialNumber(createData.serialNumber())
                .withMotorcycleClass(MotorcycleClass.valueOf(createData.motorcycleClass().toUpperCase()))
                .withCreatedAt(createData.createdAt())
                .withModifiedAt(createData.modifiedAt())
                .withIsActive(Boolean.TRUE)
                .build();
    }

    public void setUnActive() {
        this.isActive = Boolean.FALSE;
        this.modifiedAt = LocalDateTime.now();
    }

    public void mapEngine(MotorcycleMapEngineData mapEngineData) {
        this.capacity = mapEngineData.capacity();
        this.horsePower = mapEngineData.horsePower();
        this.modifiedAt = LocalDateTime.now();
    }

    public void updateOwner(MotorcycleOwnerUpdateData updateOwnerData) {
        this.bikerId = updateOwnerData.newMotorcycleOwnerId();
        this.modifiedAt = LocalDateTime.now();
    }
}