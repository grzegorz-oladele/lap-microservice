package pl.grzegorz.lapdomain.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class BikerPermissionException extends RuntimeException {

    private final String message;
}
