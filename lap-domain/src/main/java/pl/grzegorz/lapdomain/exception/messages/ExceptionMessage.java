package pl.grzegorz.lapdomain.exception.messages;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ExceptionMessage {

    BIKER_NOT_FOUND_EXCEPTION_MESSAGE("Biker using id -> [%s] not found"),
    BIKER_PERMISSION_EXCEPTION_MESSAGE("Biker using id -> [%s] hasn't permission to motorcycle using id -> [%s]"),
    BIKER_UNACTIVE_EXCEPTION_MESSAGE("Biker using id -> [%s] is unactive"),
    MOTORCYCLE_SERVICE_CONNECTION_REFUSED_MESSAGE("Trying to connect with motorcycle microservice % times. " +
            "Connection refused"),
    MOTORCYCLE_UNACTIVE_EXCEPTION_MESSAGE("Motorcycle using id -> [%s] is unactive"),
    MOTORCYCLE_NOT_FOUND_EXCEPTION_MESSAGE("Motorcycle using id -> [%s] not found"),
    CIRCUIT_NOT_FOUND_MESSAGE("Circuit using id -> [%s] not found"),
    CIRCUIT_ALREADY_EXISTS_MESSAGE("Circuit using name -> [%s] already exists"),
    INVALID_LAP_TIME_FORMAT_EXCEPTION_MESSAGE("Invalid lap time format"),
    LAP_NOT_FOUND_EXCEPTION_MESSAGE("Lap with id -> [%s] not found");

    private final String message;
}