package pl.grzegorz.lapapplication.services;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.grzegorz.lapapplication.ports.in.biker.command.BikerCreateCommandUseCase.BikerCreateCommand;
import pl.grzegorz.lapapplication.ports.in.biker.command.BikerUpdateCommandUseCase.BikerUpdateCommand;
import pl.grzegorz.lapapplication.ports.in.lap.command.LapCreateCommandUseCase.LapCreateCommand;
import pl.grzegorz.lapapplication.ports.in.motorcycle.command.MotorcycleOwnerUpdateCommandUseCase.MotorcycleOwnerUpdateCommand;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;
import pl.grzegorz.lapdomain.aggregates.CircuitAggregate;
import pl.grzegorz.lapdomain.aggregates.LapAggregate;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Fixtures {

    public static final UUID LAP_ID = UUID.fromString("9b8bb53f-8e39-46d6-bb9b-7da236a62b83");
    public static final UUID CIRCUIT_ID = UUID.fromString("98ba7243-999f-4350-810c-cf5c8b013baa");
    public static final UUID BIKER_ID = UUID.fromString("de7bb298-4d64-4f43-98cb-b340cc0feddb");
    public static final UUID SECOND_BIKER_ID = UUID.fromString("db23b58e-31e0-4c8b-b869-8d1bb7919dca");
    public static final UUID MOTORCYCLE_ID = UUID.fromString("9773aae6-ad7e-492d-b32c-5e50dd1e313b");
    public static final UUID UNACTIVE_MOTORCYCLE_ID = UUID.fromString("a86796a5-d66a-4e6f-a81c-e720fa041fd7");

    public static LapCreateCommand lapCreateCommand() {
        return LapCreateCommand.builder()
                .withLapDate(LocalDate.now())
                .withLapTime("1:34.893")
                .build();
    }

    public static LapCreateCommand invalidLapCreateCommand() {
        return LapCreateCommand.builder()
                .withLapDate(LocalDate.now())
                .withLapTime("1.34.893")
                .build();
    }

    public static LapAggregate lapAggregate() {
        return LapAggregate.builder()
                .withId(LAP_ID)
                .withLapDate(LocalDate.now())
                .withMotorcycleId(MOTORCYCLE_ID)
                .withBikerId(BIKER_ID)
                .withCircuitId(CIRCUIT_ID)
                .withModifiedAt(LocalDateTime.now())
                .withCreatedAt(LocalDateTime.now())
                .withLapTime("1:23:456")
                .build();
    }

    public static MotorcycleAggregate motorcycleAggregate() {
        return MotorcycleAggregate.builder()
                .withId(MOTORCYCLE_ID)
                .withBikerId(BIKER_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withCapacity(1199)
                .withVintage(2022)
                .withHorsePower(225)
                .withIsActive(Boolean.TRUE)
                .build();
    }
    public static MotorcycleAggregate unactiveMotorcycleAggregate() {
        return MotorcycleAggregate.builder()
                .withId(UNACTIVE_MOTORCYCLE_ID)
                .withBikerId(BIKER_ID)
                .withBrand("Kawasaki")
                .withModel("ZX6R Ninja")
                .withCapacity(599)
                .withVintage(2009)
                .withHorsePower(123)
                .withIsActive(Boolean.FALSE)
                .build();
    }

    public static BikerAggregate bikerAggregate() {
        return BikerAggregate.builder()
                .withId(BIKER_ID)
                .withIsActive(Boolean.TRUE)
                .withFirstName("Tomasz")
                .withUserName("tomasz_123")
                .withEmail("tomasz@com")
                .build();
    }

    public static BikerAggregate unactiveBikerAggregate() {
        return BikerAggregate.builder()
                .withId(BIKER_ID)
                .withIsActive(Boolean.FALSE)
                .withFirstName("Tomasz")
                .withUserName("tomasz_123")
                .withEmail("tomasz@com")
                .build();
    }

    public static CircuitAggregate circuitAggregate() {
        return CircuitAggregate.builder()
                .withId(CIRCUIT_ID)
                .withName("Tor Poznań")
                .withCity("Poznań")
                .withLength(4.087)
                .build();
    }

    public static BikerCreateCommand bikerCreateCommand() {
        return BikerCreateCommand.builder()
                .withId(BIKER_ID)
                .withUsername("tomasz_123")
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withEmail("tomasz@123.pl")
                .withEnable(Boolean.TRUE)
                .withPhoneNumber("123-456-789")
                .withDateOfBirth(LocalDate.of(1989, 2, 23).toString())
                .build();
    }

    public static BikerUpdateCommand bikerUpdateCommand() {
        return BikerUpdateCommand.builder()
                .withId(BIKER_ID)
                .withFirstName("Marian")
                .withLastName("Marianowski")
                .withEmail("marian@123.pl")
                .withEnable(Boolean.TRUE)
                .withPhoneNumber("123-456-789")
                .build();
    }

    public static MotorcycleOwnerUpdateCommand motorcycleOwnerUpdateCommand() {
        return MotorcycleOwnerUpdateCommand.builder()
                .withMotorcycleId(MOTORCYCLE_ID)
                .withPreviousOwnerId(BIKER_ID)
                .withNewOwnerId(SECOND_BIKER_ID)
                .build();
    }
}