package pl.grzegorz.lapapplication.services.query.lap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.lapapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.lapapplication.ports.out.circuit.query.CircuitByIdQueryPort;
import pl.grzegorz.lapapplication.ports.out.lap.query.LapByIdQueryPort;
import pl.grzegorz.lapapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.lapapplication.services.lap.query.LapByIdQueryService;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;
import pl.grzegorz.lapdomain.aggregates.CircuitAggregate;
import pl.grzegorz.lapdomain.aggregates.LapAggregate;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;
import static pl.grzegorz.lapapplication.services.Fixtures.*;

@ExtendWith(MockitoExtension.class)
class LapByIdQueryServiceTest {

    @InjectMocks
    private LapByIdQueryService lapByIdQueryService;
    @Mock
    private MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    @Mock
    private LapByIdQueryPort lapByIdQueryPort;
    @Mock
    private BikerByIdQueryPort bikerByIdQueryPort;
    @Mock
    private CircuitByIdQueryPort circuitByIdQueryPort;

    private LapAggregate lapAggregate;
    private BikerAggregate bikerAggregate;
    private MotorcycleAggregate motorcycleAggregate;
    private CircuitAggregate circuitAggregate;

    @BeforeEach
    void setup() {
        lapAggregate = lapAggregate();
        bikerAggregate = bikerAggregate();
        motorcycleAggregate = motorcycleAggregate();
        circuitAggregate = circuitAggregate();
    }

    @Test
    void shouldReturnLapViewByLapId() {
//        given
        var lapId = LAP_ID.toString();
        var bikerId = BIKER_ID.toString();
        var motorcycleId = MOTORCYCLE_ID.toString();
        var circuitId = CIRCUIT_ID.toString();
        when(lapByIdQueryPort.findById(lapId, bikerId)).thenReturn(lapAggregate);
        when(bikerByIdQueryPort.getById(bikerId)).thenReturn(bikerAggregate);
        when(motorcycleByIdQueryPort.execute(motorcycleId)).thenReturn(motorcycleAggregate);
        when(circuitByIdQueryPort.findById(circuitId)).thenReturn(circuitAggregate);
//        when
        var result = lapByIdQueryService.findById(lapId, bikerId);
//        then
        assertAll(
                () -> assertThat(result.id(), is(lapAggregate.id())),
                () -> assertThat(result.lapTime(), is(lapAggregate.lapTime())),
                () -> assertThat(result.lapDate(), is(lapAggregate.lapDate())),
                () -> assertThat(result.motorcycle().id(), is(motorcycleAggregate.id())),
                () -> assertThat(result.motorcycle().brand(), is(motorcycleAggregate.brand())),
                () -> assertThat(result.motorcycle().model(), is(motorcycleAggregate.model())),
                () -> assertThat(result.motorcycle().vintage(), is(motorcycleAggregate.vintage())),
                () -> assertThat(result.motorcycle().horsePower(), is(motorcycleAggregate.horsePower())),
                () -> assertThat(result.motorcycle().capacity(), is(motorcycleAggregate.capacity())),
                () -> assertThat(result.biker().id(), is(bikerAggregate.id())),
                () -> assertThat(result.biker().username(), is(bikerAggregate.userName())),
                () -> assertThat(result.biker().firstName(), is(bikerAggregate.firstName())),
                () -> assertThat(result.biker().email(), is(bikerAggregate.email())),
                () -> assertThat(result.circuit().id(), is(circuitAggregate.id())),
                () -> assertThat(result.circuit().name(), is(circuitAggregate.name())),
                () -> assertThat(result.circuit().city(), is(circuitAggregate.city())),
                () -> assertThat(result.circuit().length(), is(circuitAggregate.length()))
        );
    }
}