package pl.grzegorz.lapapplication.services.motorcycle.commad;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.lapapplication.ports.in.motorcycle.command.MotorcycleOwnerUpdateCommandUseCase;
import pl.grzegorz.lapapplication.ports.in.motorcycle.command.MotorcycleOwnerUpdateCommandUseCase.MotorcycleOwnerUpdateCommand;
import pl.grzegorz.lapapplication.ports.out.motorcycle.command.MotorcycleOwnerUpdateCommandPort;
import pl.grzegorz.lapapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.grzegorz.lapapplication.services.Fixtures.motorcycleAggregate;
import static pl.grzegorz.lapapplication.services.Fixtures.motorcycleOwnerUpdateCommand;

@ExtendWith(MockitoExtension.class)
class MotorcycleOwnerUpdateCommandServiceTest {

    @InjectMocks
    private MotorcycleOwnerUpdateCommandService motorcycleOwnerUpdateCommandService;
    @Mock
    private MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    @Mock
    private MotorcycleOwnerUpdateCommandPort motorcycleOwnerUpdateCommandPort;

    private MotorcycleOwnerUpdateCommand motorcycleOwnerUpdateCommand;
    private MotorcycleAggregate motorcycleAggregate;

    @BeforeEach
    void setup() {
        motorcycleOwnerUpdateCommand = motorcycleOwnerUpdateCommand();
        motorcycleAggregate = motorcycleAggregate();
    }

    @Test
    void shouldCallUpdateOwnerOnMotorcycleOwnerUpdateCommandPortInterface() {
//        given
        var motorcycleId = motorcycleOwnerUpdateCommand.motorcycleId().toString();
        when(motorcycleByIdQueryPort.execute(motorcycleId)).thenReturn(motorcycleAggregate);
//        when
        motorcycleOwnerUpdateCommandService.updateOwner(motorcycleOwnerUpdateCommand);
//        then
        verify(motorcycleOwnerUpdateCommandPort).updateOwner(motorcycleAggregate);
    }
}