package pl.grzegorz.lapapplication.services.biker;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.lapapplication.ports.in.biker.command.BikerCreateCommandUseCase.BikerCreateCommand;
import pl.grzegorz.lapapplication.ports.out.biker.command.BikerCreateCommandPort;
import pl.grzegorz.lapapplication.services.Fixtures;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class BikerCreateCommandServiceTest {

    @InjectMocks
    private BikerCreateCommandService bikerCreateCommandService;
    @Mock
    private BikerCreateCommandPort bikerCreateCommandPort;

    private BikerCreateCommand bikerCreateCommand;

    @BeforeEach
    void setup() {
        bikerCreateCommand = Fixtures.bikerCreateCommand();
    }

    @Test
    void shouldCallCreateMethodOnBikerCreateCommandPortInterface() {
//        given
//        when
        bikerCreateCommandService.create(bikerCreateCommand);
//        then
        verify(bikerCreateCommandPort).create(any(BikerAggregate.class));
    }
}