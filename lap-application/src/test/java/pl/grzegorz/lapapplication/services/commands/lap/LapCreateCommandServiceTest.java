package pl.grzegorz.lapapplication.services.commands.lap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.lapapplication.ports.in.lap.command.LapCreateCommandUseCase.LapCreateCommand;
import pl.grzegorz.lapapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.lapapplication.ports.out.circuit.query.CircuitByIdQueryPort;
import pl.grzegorz.lapapplication.ports.out.lap.command.LapCreateCommandPort;
import pl.grzegorz.lapapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.lapapplication.services.lap.command.LapCreateCommandService;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;
import pl.grzegorz.lapdomain.aggregates.CircuitAggregate;
import pl.grzegorz.lapdomain.aggregates.LapAggregate;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.lapdomain.exception.BikerUnactiveException;
import pl.grzegorz.lapdomain.exception.InvalidLapTimeFormatException;
import pl.grzegorz.lapdomain.exception.MotorcycleUnactiveException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.grzegorz.lapapplication.services.Fixtures.*;

@ExtendWith(MockitoExtension.class)
class LapCreateCommandServiceTest {

    @InjectMocks
    private LapCreateCommandService lapCreateCommandService;
    @Mock
    private CircuitByIdQueryPort circuitByIdQueryPort;
    @Mock
    private BikerByIdQueryPort bikerByIdQueryPort;
    @Mock
    private MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    @Mock
    private LapCreateCommandPort lapCreateCommandPort;

    private LapCreateCommand lapCreateCommand;
    private LapCreateCommand invalidLapCreateCommand;
    private CircuitAggregate circuitAggregate;
    private MotorcycleAggregate motorcycleAggregate;
    private MotorcycleAggregate unactiveMotorcycleAggregate;
    private BikerAggregate bikerAggregate;
    private BikerAggregate unactiveBikerAggregate;

    @BeforeEach
    void setup() {
        lapCreateCommand = lapCreateCommand();
        invalidLapCreateCommand = invalidLapCreateCommand();
        bikerAggregate = bikerAggregate();
        unactiveBikerAggregate = unactiveBikerAggregate();
        motorcycleAggregate = motorcycleAggregate();
        unactiveMotorcycleAggregate = unactiveMotorcycleAggregate();
        circuitAggregate = circuitAggregate();
    }

    @Test
    void shouldCallCreateMethodFromLapCreateCommandPort() {
//        given
        var circuitId = CIRCUIT_ID.toString();
        var bikerId = BIKER_ID.toString();
        var motorcycleId = MOTORCYCLE_ID.toString();
        when(circuitByIdQueryPort.findById(circuitId)).thenReturn(circuitAggregate);
        when(bikerByIdQueryPort.getById(bikerId)).thenReturn(bikerAggregate);
        when(motorcycleByIdQueryPort.execute(motorcycleId)).thenReturn(motorcycleAggregate);
//        when
        lapCreateCommandService.create(circuitId, bikerId, motorcycleId, lapCreateCommand);
//        then
        verify(lapCreateCommandPort).create(any(LapAggregate.class));
    }

    @Test
    void shouldThrowInvalidLapTimeFormatExceptionWhenLapTimeValueWillBeInvalid() {
//        given
        var circuitId = CIRCUIT_ID.toString();
        var bikerId = BIKER_ID.toString();
        var motorcycleId = MOTORCYCLE_ID.toString();
//        when + then
        assertThrows(InvalidLapTimeFormatException.class,
                () -> lapCreateCommandService.create(circuitId, bikerId, motorcycleId, invalidLapCreateCommand));
    }

    @Test
    void shouldThrowBikerUnactiveExceptionWhenBikerWillBeUnactive() {
//        given
        var circuitId = CIRCUIT_ID.toString();
        var bikerId = BIKER_ID.toString();
        var motorcycleId = MOTORCYCLE_ID.toString();
        when(bikerByIdQueryPort.getById(bikerId)).thenReturn(unactiveBikerAggregate);
        when(circuitByIdQueryPort.findById(circuitId)).thenReturn(circuitAggregate);
//        when + then
        assertThrows(BikerUnactiveException.class,
                () -> lapCreateCommandService.create(circuitId, bikerId, motorcycleId, lapCreateCommand));
    }

    @Test
    void shouldThrowMotorcycleUnactiveExceptionWneMotorcycleWillBeUnactive() {
        var circuitId = CIRCUIT_ID.toString();
        var bikerId = BIKER_ID.toString();
        var motorcycleId = UNACTIVE_MOTORCYCLE_ID.toString();
        when(motorcycleByIdQueryPort.execute(motorcycleId)).thenReturn(unactiveMotorcycleAggregate);
        when(circuitByIdQueryPort.findById(circuitId)).thenReturn(circuitAggregate);
        when(bikerByIdQueryPort.getById(bikerId)).thenReturn(bikerAggregate);
//        when + then
        assertThrows(MotorcycleUnactiveException.class,
                () -> lapCreateCommandService.create(circuitId, bikerId, motorcycleId, lapCreateCommand));
    }
}