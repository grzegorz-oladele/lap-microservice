package pl.grzegorz.lapapplication.services.biker;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.lapapplication.ports.in.biker.command.BikerUpdateCommandUseCase.BikerUpdateCommand;
import pl.grzegorz.lapapplication.ports.out.biker.command.BikerUpdateCommandPort;
import pl.grzegorz.lapapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.lapapplication.services.Fixtures;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BikerUpdateCommandServiceTest {

    @InjectMocks
    private BikerUpdateCommandService bikerUpdateCommandService;
    @Mock
    private BikerUpdateCommandPort bikerUpdateCommandPort;
    @Mock
    private BikerByIdQueryPort bikerByIdQueryPort;

    private BikerUpdateCommand bikerUpdateCommand;
    private BikerAggregate bikerAggregate;

    @BeforeEach
    void setup() {
        bikerUpdateCommand = Fixtures.bikerUpdateCommand();
        bikerAggregate = Fixtures.bikerAggregate();
    }

    @Test
    void shouldCallUpdateMethodOnBikerUpdateCommandPortInterface() {
//        given
        var bikerId = bikerAggregate.id();
        when(bikerByIdQueryPort.getById(bikerId.toString())).thenReturn(bikerAggregate);
//        when
        bikerUpdateCommandService.update(bikerUpdateCommand);
//        then
        verify(bikerUpdateCommandPort).update(any(BikerAggregate.class));
    }
}