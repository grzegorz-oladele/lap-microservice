package pl.grzegorz.lapapplication.services.biker;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.lapapplication.ports.out.biker.command.BikerEnableCommandPort;
import pl.grzegorz.lapapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.lapapplication.services.Fixtures;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BikerEnableCommandServiceTest {

    @InjectMocks
    private BikerEnableCommandService bikerEnableCommandService;
    @Mock
    private BikerByIdQueryPort bikerByIdQueryPort;
    @Mock
    private BikerEnableCommandPort bikerEnableCommandPort;

    private BikerAggregate bikerAggregate;

    @BeforeEach
    void setup() {
        bikerAggregate = Fixtures.bikerAggregate();
    }

    @Test
    void shouldCallEnableMethodOnBikerEnableCommandPortInterface() {
//        given
        var bikerId = bikerAggregate.id();
        when(bikerByIdQueryPort.getById(bikerId.toString())).thenReturn(bikerAggregate);
//        when
        bikerEnableCommandService.enable(bikerId);
//        then
        verify(bikerEnableCommandPort).enable(bikerAggregate);
    }
}