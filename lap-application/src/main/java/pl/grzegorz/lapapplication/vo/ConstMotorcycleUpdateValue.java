package pl.grzegorz.lapapplication.vo;

import lombok.AccessLevel;
import lombok.Builder;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.lapdomain.data.motorcycle.MotorcycleClass;

import java.time.LocalDateTime;

@Builder(setterPrefix = "with", access = AccessLevel.PRIVATE)
public record ConstMotorcycleUpdateValue(
        String brand,
        String model,
        int vintage,
        MotorcycleClass motorcycleClass,
        String serialNumber,
        LocalDateTime createdAt
) {

    public static ConstMotorcycleUpdateValue toConstUpdateValue(MotorcycleAggregate motorcycleAggregate) {
        return ConstMotorcycleUpdateValue.builder()
                .withBrand(motorcycleAggregate.brand())
                .withModel(motorcycleAggregate.model())
                .withVintage(motorcycleAggregate.vintage())
                .withSerialNumber(motorcycleAggregate.serialNumber())
                .withCreatedAt(motorcycleAggregate.createdAt())
                .withMotorcycleClass(motorcycleAggregate.motorcycleClass())
                .build();
    }
}