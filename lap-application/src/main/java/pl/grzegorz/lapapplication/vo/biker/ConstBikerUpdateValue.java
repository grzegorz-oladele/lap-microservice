package pl.grzegorz.lapapplication.vo.biker;

import lombok.AccessLevel;
import lombok.Builder;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Builder(toBuilder = true, setterPrefix = "with", access = AccessLevel.PRIVATE)
public record ConstBikerUpdateValue(
        LocalDateTime createdAt,
        String username,
        LocalDate dateOfBirth
) {

    public static ConstBikerUpdateValue toConstBikerUpdateValue(BikerAggregate bikerAggregate) {
        return ConstBikerUpdateValue.builder()
                .withCreatedAt(bikerAggregate.createdAt())
                .withUsername(bikerAggregate.userName())
                .withDateOfBirth(bikerAggregate.dateOfBirth())
                .build();
    }
}