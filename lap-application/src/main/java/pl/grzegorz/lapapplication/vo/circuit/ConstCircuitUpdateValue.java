package pl.grzegorz.lapapplication.vo.circuit;

import lombok.AccessLevel;
import lombok.Builder;
import pl.grzegorz.lapdomain.aggregates.CircuitAggregate;

import java.time.LocalDateTime;

@Builder(setterPrefix = "with", access = AccessLevel.PRIVATE)
public record ConstCircuitUpdateValue(
        String city,
        LocalDateTime createdAt
) {

    public static ConstCircuitUpdateValue toUpdateValue(CircuitAggregate aggregate) {
        return ConstCircuitUpdateValue.builder()
                .withCity(aggregate.city())
                .withCreatedAt(aggregate.createdAt())
                .build();
    }
}