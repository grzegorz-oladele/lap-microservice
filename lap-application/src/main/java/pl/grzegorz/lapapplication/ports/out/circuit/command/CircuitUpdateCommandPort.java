package pl.grzegorz.lapapplication.ports.out.circuit.command;

import pl.grzegorz.lapdomain.aggregates.CircuitAggregate;

public interface CircuitUpdateCommandPort {

    void update(CircuitAggregate circuitAggregate);
}