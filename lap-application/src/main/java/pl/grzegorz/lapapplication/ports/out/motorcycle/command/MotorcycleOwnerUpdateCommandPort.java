package pl.grzegorz.lapapplication.ports.out.motorcycle.command;

import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;

public interface MotorcycleOwnerUpdateCommandPort {

    void updateOwner(MotorcycleAggregate motorcycleAggregate);
}