package pl.grzegorz.lapapplication.ports.out.motorcycle.command;

import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;

public interface MotorcycleMapEngineCommandPort {

    void map(MotorcycleAggregate motorcycleAggregate);
}