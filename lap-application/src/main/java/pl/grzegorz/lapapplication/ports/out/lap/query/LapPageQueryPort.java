package pl.grzegorz.lapapplication.ports.out.lap.query;

import pl.grzegorz.lapapplication.resources.ResourceFilter;
import pl.grzegorz.lapapplication.resources.ResultView;
import pl.grzegorz.lapdomain.aggregates.LapAggregate;

public interface LapPageQueryPort {

    ResultView<LapAggregate> findAll(ResourceFilter filter);
}