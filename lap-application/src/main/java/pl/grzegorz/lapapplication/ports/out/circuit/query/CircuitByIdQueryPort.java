package pl.grzegorz.lapapplication.ports.out.circuit.query;

import pl.grzegorz.lapdomain.aggregates.CircuitAggregate;

public interface CircuitByIdQueryPort {

    CircuitAggregate findById(String circuitId);
}
