package pl.grzegorz.lapapplication.ports.out.biker.command;

import pl.grzegorz.lapdomain.aggregates.BikerAggregate;

public interface BikerEnableCommandPort {

    void enable(BikerAggregate bikerAggregate);
}