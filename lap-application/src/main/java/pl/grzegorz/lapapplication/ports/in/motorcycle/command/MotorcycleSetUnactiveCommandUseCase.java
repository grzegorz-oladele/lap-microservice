package pl.grzegorz.lapapplication.ports.in.motorcycle.command;

public interface MotorcycleSetUnactiveCommandUseCase {

    void execute(String motorcycleId);
}