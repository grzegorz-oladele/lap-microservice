package pl.grzegorz.lapapplication.ports.in.lap.query;

import pl.grzegorz.lapapplication.ports.view.LapView;

public interface LapByIdQueryUseCase {

    LapView findById(String lapId, String bikerId);
}