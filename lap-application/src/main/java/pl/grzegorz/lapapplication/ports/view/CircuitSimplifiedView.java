package pl.grzegorz.lapapplication.ports.view;

import lombok.AccessLevel;
import lombok.Builder;
import pl.grzegorz.lapdomain.aggregates.CircuitAggregate;

import java.util.UUID;

@Builder(setterPrefix = "with", access = AccessLevel.PRIVATE)
public record CircuitSimplifiedView(
        UUID id,
        String name,
        String city,
        double length
) {

    public static CircuitSimplifiedView toView(CircuitAggregate circuitAggregate) {
        return CircuitSimplifiedView.builder()
                .withId(circuitAggregate.id())
                .withName(circuitAggregate.name())
                .withCity(circuitAggregate.city())
                .withLength(circuitAggregate.length())
                .build();
    }
}
