package pl.grzegorz.lapapplication.ports.in.circuit.command;

import lombok.Builder;
import pl.grzegorz.lapdomain.data.circuit.CircuitCreateData;

import java.time.LocalDateTime;
import java.util.UUID;

public interface CircuitCreateCommandUseCase {

    void create(CircuitCreateCommand command);

    @Builder(setterPrefix = "with")
    record CircuitCreateCommand(
            UUID id,
            String name,
            String description,
            String city,
            String postalCode,
            String streetName,
            String streetNumber,
            double length,
            String email,
            String phoneNumber
    ) {

        public CircuitCreateData toCreateData() {
            return CircuitCreateData.builder()
                    .withId(id)
                    .withName(name)
                    .withDescription(description)
                    .withCity(city)
                    .withPostalCode(postalCode)
                    .withStreetName(streetName)
                    .withStreetNumber(streetNumber)
                    .withLength(length)
                    .withEmail(email)
                    .withPhoneNumber(phoneNumber)
                    .withCreatedAt(LocalDateTime.now())
                    .withModifiedAt(LocalDateTime.now())
                    .build();
        }
    }
}