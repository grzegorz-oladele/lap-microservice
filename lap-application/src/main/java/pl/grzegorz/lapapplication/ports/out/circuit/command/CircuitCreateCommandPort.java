package pl.grzegorz.lapapplication.ports.out.circuit.command;

import pl.grzegorz.lapdomain.aggregates.CircuitAggregate;

public interface CircuitCreateCommandPort {

    void create(CircuitAggregate circuitAggregate);
}