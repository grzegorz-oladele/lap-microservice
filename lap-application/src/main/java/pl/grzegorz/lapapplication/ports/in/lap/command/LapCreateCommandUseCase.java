package pl.grzegorz.lapapplication.ports.in.lap.command;

import lombok.Builder;
import pl.grzegorz.lapdomain.data.lap.LapCreateData;
import pl.grzegorz.lapdomain.exception.InvalidLapTimeFormatException;
import pl.grzegorz.lapdomain.exception.messages.ExceptionMessage;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

public interface LapCreateCommandUseCase {

    UUID create(String circuitId, String bikerId, String motorcycleId, LapCreateCommand command) throws ExecutionException, InterruptedException;

    @Builder(setterPrefix = "with")
    record LapCreateCommand(
            String lapTime,
            LocalDate lapDate
    ) {

        private static final String LAP_TIME_REGEXP = "^(?:[0-5]?[0-9]):(?:[0-5][0-9])\\.(?:\\d{3})$";
        private static final Pattern LAP_TIME_PATTERN = Pattern.compile(LAP_TIME_REGEXP);

        public LapCreateData lapCreateData() {
            return LapCreateData.builder()
                    .withLapTime(validateLapTime())
                    .withLapDate(lapDate)
                    .withCreatedAt(LocalDateTime.now())
                    .withModifiedAt(LocalDateTime.now())
                    .build();
        }

        private String validateLapTime() {
            if (!LAP_TIME_PATTERN.matcher(lapTime).matches()) {
                throw new InvalidLapTimeFormatException(
                        ExceptionMessage.INVALID_LAP_TIME_FORMAT_EXCEPTION_MESSAGE.getMessage());
            }
            return lapTime;
        }
    }
}