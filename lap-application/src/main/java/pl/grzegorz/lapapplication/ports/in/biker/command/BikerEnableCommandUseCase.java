package pl.grzegorz.lapapplication.ports.in.biker.command;

import java.util.UUID;

public interface BikerEnableCommandUseCase {

    void enable(UUID bikerId);
}