package pl.grzegorz.lapapplication.ports.out.motorcycle.query;

import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;

public interface MotorcycleByIdQueryPort {

    MotorcycleAggregate execute(String motorcycleId);
}