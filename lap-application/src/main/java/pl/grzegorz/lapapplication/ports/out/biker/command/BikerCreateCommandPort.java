package pl.grzegorz.lapapplication.ports.out.biker.command;

import pl.grzegorz.lapdomain.aggregates.BikerAggregate;

public interface BikerCreateCommandPort {

    void create(BikerAggregate bikerAggregate);
}