package pl.grzegorz.lapapplication.ports.view;

import lombok.Builder;
import pl.grzegorz.lapdomain.aggregates.LapAggregate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Builder(setterPrefix = "with", toBuilder = true)
public record LapView(
        UUID id,
        String lapTime,
        LocalDate lapDate,
        BikerView biker,
        CircuitSimplifiedView circuit,
        MotorcycleView motorcycle,
        LocalDateTime createdAt,
        LocalDateTime modifiedAt
) {

    public static LapView toView(LapAggregate aggregate) {
        return LapView.builder()
                .withId(aggregate.id())
                .withLapTime(aggregate.lapTime())
                .withLapDate(aggregate.lapDate())
                .withCreatedAt(aggregate.createdAt())
                .withModifiedAt(aggregate.modifiedAt())
                .build();
    }
}