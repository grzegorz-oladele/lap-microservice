package pl.grzegorz.lapapplication.ports.view;

import lombok.Builder;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;

import java.util.UUID;

@Builder(setterPrefix = "with")
public record BikerView(
        UUID id,
        String firstName,
        String username,
        String email
) {

    public static BikerView toView(BikerAggregate bikerAggregate) {
        return BikerView.builder()
                .withId(bikerAggregate.id())
                .withFirstName(bikerAggregate.firstName())
                .withUsername(bikerAggregate.userName())
                .withEmail(bikerAggregate.email())
                .build();
    }
}
