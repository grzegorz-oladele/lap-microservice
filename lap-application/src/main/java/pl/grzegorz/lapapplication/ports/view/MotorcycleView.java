package pl.grzegorz.lapapplication.ports.view;

import lombok.Builder;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;

import java.util.UUID;

@Builder(setterPrefix = "with")
public record MotorcycleView(
        UUID id,
        String brand,
        String model,
        int capacity,
        int horsePower,
        int vintage
) {

    public static MotorcycleView toView(MotorcycleAggregate motorcycleAggregate) {
        return MotorcycleView.builder()
                .withId(motorcycleAggregate.id())
                .withBrand(motorcycleAggregate.brand())
                .withModel(motorcycleAggregate.model())
                .withCapacity(motorcycleAggregate.capacity())
                .withHorsePower(motorcycleAggregate.horsePower())
                .withVintage(motorcycleAggregate.vintage())
                .build();
    }
}