package pl.grzegorz.lapapplication.ports.in.biker.command;

import lombok.Builder;
import pl.grzegorz.lapdomain.data.biker.BikerCreateData;

import java.time.LocalDateTime;
import java.util.UUID;

public interface BikerCreateCommandUseCase {

    void create(BikerCreateCommand createCommand);

    @Builder(toBuilder = true, setterPrefix = "with")
    record BikerCreateCommand(
            UUID id,
            String username,
            String firstName,
            String lastName,
            String email,
            Boolean enable,
            String phoneNumber,
            String dateOfBirth
    ) {
        public BikerCreateData toCreateData() {
            return BikerCreateData.builder()
                    .withId(id)
                    .withUsername(username)
                    .withFirstName(firstName)
                    .withLastName(lastName)
                    .withEmail(email)
                    .withEnable(Boolean.FALSE)
                    .withPhoneNumber(phoneNumber)
                    .withDateOfBirth(dateOfBirth)
                    .withCreatedAt(LocalDateTime.now())
                    .withModifiedAt(LocalDateTime.now())
                    .build();
        }
    }
}