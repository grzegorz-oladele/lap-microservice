package pl.grzegorz.lapapplication.ports.in.motorcycle.command;

import lombok.Builder;
import pl.grzegorz.lapdomain.data.MotorcycleOwnerUpdateData;

import java.util.UUID;

public interface MotorcycleOwnerUpdateCommandUseCase {

    void updateOwner(MotorcycleOwnerUpdateCommand motorcycleOwnerUpdateCommand);

    @Builder(setterPrefix = "with")
    record MotorcycleOwnerUpdateCommand(
            UUID motorcycleId,
            UUID previousOwnerId,
            UUID newOwnerId
    ) {

        public MotorcycleOwnerUpdateData toOwnerUpdateData() {
            return new MotorcycleOwnerUpdateData(newOwnerId);
        }
    }
}