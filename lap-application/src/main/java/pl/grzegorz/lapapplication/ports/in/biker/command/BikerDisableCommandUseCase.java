package pl.grzegorz.lapapplication.ports.in.biker.command;

import java.util.UUID;

public interface BikerDisableCommandUseCase {

    void disable(UUID bikerId);
}