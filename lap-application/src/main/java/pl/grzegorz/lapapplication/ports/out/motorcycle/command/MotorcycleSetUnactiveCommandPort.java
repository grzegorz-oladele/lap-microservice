package pl.grzegorz.lapapplication.ports.out.motorcycle.command;

import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;

public interface MotorcycleSetUnactiveCommandPort {

    void execute(MotorcycleAggregate motorcycleAggregate);
}