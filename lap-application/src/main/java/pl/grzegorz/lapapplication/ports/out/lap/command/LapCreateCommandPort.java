package pl.grzegorz.lapapplication.ports.out.lap.command;

import pl.grzegorz.lapdomain.aggregates.LapAggregate;

import java.util.UUID;

public interface LapCreateCommandPort {

    UUID create(LapAggregate lapAggregate);
}