package pl.grzegorz.lapapplication.ports.in.circuit.command;

import lombok.Builder;
import pl.grzegorz.lapapplication.vo.circuit.ConstCircuitUpdateValue;
import pl.grzegorz.lapdomain.data.circuit.CircuitUpdateData;

import java.time.LocalDateTime;
import java.util.UUID;

public interface CircuitUpdateCommandUseCase {

    void update(CircuitUpdateCommand command);

    @Builder(setterPrefix = "with")
    record CircuitUpdateCommand(
            UUID id,
            String name,
            String description,
            String postalCode,
            String streetName,
            String streetNumber,
            double length,
            String email,
            String phoneNumber
    ) {

        public CircuitUpdateData toUpdateData(ConstCircuitUpdateValue constUpdateValue) {
            return CircuitUpdateData.builder()
                    .withId(id)
                    .withName(name)
                    .withDescription(description)
                    .withCity(constUpdateValue.city())
                    .withPostalCode(postalCode)
                    .withStreetName(streetName)
                    .withStreetNumber(streetNumber)
                    .withLength(length)
                    .withEmail(email)
                    .withPhoneNumber(phoneNumber)
                    .withCreatedAt(constUpdateValue.createdAt())
                    .withModifiedAt(LocalDateTime.now())
                    .build();
        }
    }
}