package pl.grzegorz.lapapplication.ports.out.lap.query;

import pl.grzegorz.lapdomain.aggregates.LapAggregate;

public interface LapByIdQueryPort {

    LapAggregate findById(String lapId, String bikerId);
}