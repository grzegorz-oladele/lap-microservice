package pl.grzegorz.lapapplication.ports.in.lap.query;

import pl.grzegorz.lapapplication.ports.view.LapView;
import pl.grzegorz.lapapplication.resources.ResourceFilter;
import pl.grzegorz.lapapplication.resources.ResultView;

public interface LapPageQueryUseCase {

    ResultView<LapView> findAll(ResourceFilter filter);
}