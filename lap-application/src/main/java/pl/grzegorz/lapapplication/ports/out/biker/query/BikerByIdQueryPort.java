package pl.grzegorz.lapapplication.ports.out.biker.query;

import pl.grzegorz.lapdomain.aggregates.BikerAggregate;

public interface BikerByIdQueryPort {

    BikerAggregate getById(String bikerId);
}