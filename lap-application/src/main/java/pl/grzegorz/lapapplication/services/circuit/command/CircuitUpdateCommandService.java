package pl.grzegorz.lapapplication.services.circuit.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.lapapplication.ports.in.circuit.command.CircuitUpdateCommandUseCase;
import pl.grzegorz.lapapplication.ports.out.circuit.command.CircuitUpdateCommandPort;
import pl.grzegorz.lapapplication.ports.out.circuit.query.CircuitByIdQueryPort;
import pl.grzegorz.lapapplication.vo.circuit.ConstCircuitUpdateValue;
import pl.grzegorz.lapdomain.aggregates.CircuitAggregate;

@RequiredArgsConstructor
public class CircuitUpdateCommandService implements CircuitUpdateCommandUseCase {

    private final CircuitByIdQueryPort circuitByIdQueryPort;
    private final CircuitUpdateCommandPort circuitUpdateCommandPort;

    @Override
    public void update(CircuitUpdateCommand command) {
        var constCircuitUpdateValue = toConstUpdateValue(command.id().toString());
        var circuitAggregate = CircuitAggregate.update(command.toUpdateData(constCircuitUpdateValue));
        circuitUpdateCommandPort.update(circuitAggregate);
    }

    private ConstCircuitUpdateValue toConstUpdateValue(String circuitId) {
        var circuitAggregate = circuitByIdQueryPort.findById(circuitId);
        return ConstCircuitUpdateValue.toUpdateValue(circuitAggregate);
    }
}