package pl.grzegorz.lapapplication.services.circuit.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.lapapplication.ports.in.circuit.command.CircuitCreateCommandUseCase;
import pl.grzegorz.lapapplication.ports.out.circuit.command.CircuitCreateCommandPort;
import pl.grzegorz.lapdomain.aggregates.CircuitAggregate;

@RequiredArgsConstructor
public class CircuitCreateCommandService implements CircuitCreateCommandUseCase {

    private final CircuitCreateCommandPort circuitCreateCommandPort;

    @Override
    public void create(CircuitCreateCommand command) {
        var circuitAggregate = CircuitAggregate.create(command.toCreateData());
        circuitCreateCommandPort.create(circuitAggregate);
    }
}