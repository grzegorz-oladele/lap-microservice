package pl.grzegorz.lapapplication.services.lap.command;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.lapapplication.ports.in.lap.command.LapCreateCommandUseCase;
import pl.grzegorz.lapapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.lapapplication.ports.out.circuit.query.CircuitByIdQueryPort;
import pl.grzegorz.lapapplication.ports.out.lap.command.LapCreateCommandPort;
import pl.grzegorz.lapapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;
import pl.grzegorz.lapdomain.aggregates.LapAggregate;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.lapdomain.exception.BikerPermissionException;
import pl.grzegorz.lapdomain.exception.BikerUnactiveException;
import pl.grzegorz.lapdomain.exception.MotorcycleUnactiveException;
import pl.grzegorz.lapdomain.exception.messages.ExceptionMessage;

import java.util.UUID;

@RequiredArgsConstructor
public class LapCreateCommandService implements LapCreateCommandUseCase {

    private final CircuitByIdQueryPort circuitByIdQueryPort;
    private final BikerByIdQueryPort bikerByIdQueryPort;
    private final MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    private final LapCreateCommandPort lapCreateCommandPort;

    @Override
    public UUID create(String circuitId, String bikerId, String motorcycleId, LapCreateCommand command) {
        var lapAggregate = LapAggregate.create(command.lapCreateData())
                .toBuilder()
                .withCircuitId(circuitByIdQueryPort.findById(circuitId).id())
                .withBikerId(getBikerId(bikerId))
                .withMotorcycleId(getMotorcycleId(motorcycleId, bikerId))
                .build();
        return lapCreateCommandPort.create(lapAggregate);
    }

    private UUID getBikerId(String bikerId) {
        var bikerAggregate = bikerByIdQueryPort.getById(bikerId);
        validateUserActivity(bikerId, bikerAggregate);
        return bikerAggregate.id();
    }

    private UUID getMotorcycleId(String motorcycleId, String bikerId) {
        var motorcycleAggregate = motorcycleByIdQueryPort.execute(motorcycleId);
        validateBikerPermission(motorcycleId, bikerId, motorcycleAggregate);
        validateMotorcycleActivity(motorcycleId, motorcycleAggregate);
        return motorcycleAggregate.id();
    }

    private void validateMotorcycleActivity(String motorcycleId, MotorcycleAggregate motorcycleAggregate) {
        if (motorcycleAggregate.isActive().equals(Boolean.FALSE)) {
            throw new MotorcycleUnactiveException(String.format(
                    ExceptionMessage.MOTORCYCLE_UNACTIVE_EXCEPTION_MESSAGE.getMessage(), motorcycleId));
        }
    }

    private void validateUserActivity(String bikerId, BikerAggregate bikerAggregate) {
        if (bikerAggregate.isActive().equals(Boolean.FALSE)) {
            throw new BikerUnactiveException(String.format(
                    ExceptionMessage.BIKER_UNACTIVE_EXCEPTION_MESSAGE.getMessage(), bikerId
            ));
        }
    }

    private void validateBikerPermission(String motorcycleId, String bikerId,
                                         MotorcycleAggregate motorcycleAggregate) {
        if (!motorcycleAggregate.bikerId().toString().equals(bikerId)) {
            throw new BikerPermissionException(
                    String.format(ExceptionMessage.BIKER_PERMISSION_EXCEPTION_MESSAGE.getMessage(), bikerId,
                            motorcycleId)
            );
        }
    }
}