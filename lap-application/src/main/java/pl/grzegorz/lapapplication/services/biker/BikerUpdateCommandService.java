package pl.grzegorz.lapapplication.services.biker;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.lapapplication.ports.in.biker.command.BikerUpdateCommandUseCase;
import pl.grzegorz.lapapplication.ports.out.biker.command.BikerUpdateCommandPort;
import pl.grzegorz.lapapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.lapapplication.vo.biker.ConstBikerUpdateValue;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;

import static pl.grzegorz.lapapplication.vo.biker.ConstBikerUpdateValue.toConstBikerUpdateValue;

@RequiredArgsConstructor
public class BikerUpdateCommandService implements BikerUpdateCommandUseCase {

    private final BikerUpdateCommandPort bikerUpdateCommandPort;
    private final BikerByIdQueryPort bikerByIdQueryPort;

    @Override
    public void update(BikerUpdateCommand updateCommand) {
        ConstBikerUpdateValue constBikerUpdateValue = toConstUpdateValue(updateCommand.id().toString());
        var bikerAggregate = BikerAggregate.update(updateCommand.toUpdateData(constBikerUpdateValue));
        bikerUpdateCommandPort.update(bikerAggregate);
    }

    private ConstBikerUpdateValue toConstUpdateValue(String bikerId) {
        BikerAggregate bikerAggregate = bikerByIdQueryPort.getById(bikerId);
        return toConstBikerUpdateValue(bikerAggregate);
    }
}