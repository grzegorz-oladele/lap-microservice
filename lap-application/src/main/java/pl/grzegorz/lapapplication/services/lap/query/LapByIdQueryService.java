package pl.grzegorz.lapapplication.services.lap.query;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.lapapplication.ports.in.lap.query.LapByIdQueryUseCase;
import pl.grzegorz.lapapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.lapapplication.ports.out.circuit.query.CircuitByIdQueryPort;
import pl.grzegorz.lapapplication.ports.out.lap.query.LapByIdQueryPort;
import pl.grzegorz.lapapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.lapapplication.ports.view.BikerView;
import pl.grzegorz.lapapplication.ports.view.CircuitSimplifiedView;
import pl.grzegorz.lapapplication.ports.view.LapView;
import pl.grzegorz.lapapplication.ports.view.MotorcycleView;

@RequiredArgsConstructor
public class LapByIdQueryService implements LapByIdQueryUseCase {

    private final LapByIdQueryPort lapByIdQueryPort;
    private final BikerByIdQueryPort bikerByIdQueryPort;
    private final MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    private final CircuitByIdQueryPort circuitByIdQueryPort;

    @Override
    public LapView findById(String lapId, String bikerId) {
        var lapAggregate = lapByIdQueryPort.findById(lapId, bikerId);
        return LapView.toView(lapAggregate).toBuilder()
                .withMotorcycle(MotorcycleView.toView(motorcycleByIdQueryPort.execute(
                        lapAggregate.motorcycleId().toString())))
                .withBiker(BikerView.toView(bikerByIdQueryPort.getById(lapAggregate.bikerId().toString())))
                .withCircuit(CircuitSimplifiedView.toView(circuitByIdQueryPort
                        .findById(lapAggregate.circuitId().toString())))
                .build();
    }
}