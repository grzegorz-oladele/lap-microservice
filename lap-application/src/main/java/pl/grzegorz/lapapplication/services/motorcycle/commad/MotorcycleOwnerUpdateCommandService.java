package pl.grzegorz.lapapplication.services.motorcycle.commad;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.lapapplication.ports.in.motorcycle.command.MotorcycleOwnerUpdateCommandUseCase;
import pl.grzegorz.lapapplication.ports.out.motorcycle.command.MotorcycleOwnerUpdateCommandPort;
import pl.grzegorz.lapapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;

@RequiredArgsConstructor
public class MotorcycleOwnerUpdateCommandService implements MotorcycleOwnerUpdateCommandUseCase {

    private final MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    private final MotorcycleOwnerUpdateCommandPort motorcycleOwnerUpdateCommandPort;

    @Override
    public void updateOwner(MotorcycleOwnerUpdateCommand motorcycleOwnerUpdateCommand) {
        var motorcycleAggregate = motorcycleByIdQueryPort.execute(motorcycleOwnerUpdateCommand.motorcycleId().toString());
        motorcycleAggregate.updateOwner(motorcycleOwnerUpdateCommand.toOwnerUpdateData());
        motorcycleOwnerUpdateCommandPort.updateOwner(motorcycleAggregate);
    }
}