package pl.grzegorz.lapapplication.services.motorcycle.commad;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.lapapplication.ports.in.motorcycle.command.MotorcycleMapEngineCommandUseCase;
import pl.grzegorz.lapapplication.ports.out.motorcycle.command.MotorcycleMapEngineCommandPort;
import pl.grzegorz.lapapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.lapdomain.exception.BikerPermissionException;
import pl.grzegorz.lapdomain.exception.messages.ExceptionMessage;

@RequiredArgsConstructor
public class MotorcycleMapEngineCommandService implements MotorcycleMapEngineCommandUseCase {

    private final MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    private final MotorcycleMapEngineCommandPort motorcycleMapEngineCommandPort;

    @Override
    public void map(MotorcycleMapEngineCommand mapEngineCommand) {
        var motorcycleId = mapEngineCommand.id().toString();
        var bikerId = mapEngineCommand.bikerId().toString();
        var motorcycleAggregate = motorcycleByIdQueryPort.execute(motorcycleId);
        validateBikerPermission(motorcycleAggregate, bikerId);
        motorcycleAggregate.mapEngine(mapEngineCommand.toMapEngineData());
        motorcycleMapEngineCommandPort.map(motorcycleAggregate);
    }

    private void validateBikerPermission(MotorcycleAggregate motorcycleAggregate, String bikerId) {
        if (!motorcycleAggregate.bikerId().toString().equals(bikerId)) {
            throw new BikerPermissionException(String.format(
                    ExceptionMessage.BIKER_PERMISSION_EXCEPTION_MESSAGE.getMessage(), bikerId, motorcycleAggregate.id()));
        }
    }
}