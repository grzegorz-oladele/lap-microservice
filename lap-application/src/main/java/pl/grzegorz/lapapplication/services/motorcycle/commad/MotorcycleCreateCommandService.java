package pl.grzegorz.lapapplication.services.motorcycle.commad;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.lapapplication.ports.in.motorcycle.command.MotorcycleCreateCommandUseCase;
import pl.grzegorz.lapapplication.ports.out.motorcycle.command.MotorcycleCreateCommandPort;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;

@RequiredArgsConstructor
public class MotorcycleCreateCommandService implements MotorcycleCreateCommandUseCase {

    private final MotorcycleCreateCommandPort motorcycleCreateCommandPort;

    @Override
    public void create(MotorcycleCreateCommand createCommand) {
        motorcycleCreateCommandPort.create(MotorcycleAggregate.create(createCommand.toCreateData()));
    }
}