package pl.grzegorz.lapapplication.services.motorcycle.commad;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.lapapplication.ports.in.motorcycle.command.MotorcycleSetUnactiveCommandUseCase;
import pl.grzegorz.lapapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.lapapplication.ports.out.motorcycle.command.MotorcycleSetUnactiveCommandPort;

@RequiredArgsConstructor
public class MotorcycleSetUnactiveCommandService implements MotorcycleSetUnactiveCommandUseCase {

    private final MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    private final MotorcycleSetUnactiveCommandPort motorcycleSetUnactiveCommandPort;
    @Override
    public void execute(String motorcycleId) {
        var motorcycleAggregate = motorcycleByIdQueryPort.execute(motorcycleId);
        motorcycleSetUnactiveCommandPort.execute(motorcycleAggregate);
    }
}