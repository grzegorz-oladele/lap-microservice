package pl.grzegorz.lapapplication.services.lap.query;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.lapapplication.ports.in.lap.query.LapPageQueryUseCase;
import pl.grzegorz.lapapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.lapapplication.ports.out.circuit.query.CircuitByIdQueryPort;
import pl.grzegorz.lapapplication.ports.out.lap.query.LapPageQueryPort;
import pl.grzegorz.lapapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.lapapplication.ports.view.BikerView;
import pl.grzegorz.lapapplication.ports.view.CircuitSimplifiedView;
import pl.grzegorz.lapapplication.ports.view.LapView;
import pl.grzegorz.lapapplication.ports.view.MotorcycleView;
import pl.grzegorz.lapapplication.resources.ResourceFilter;
import pl.grzegorz.lapapplication.resources.ResultView;

import java.util.stream.Collectors;

@RequiredArgsConstructor
public class LapPageQueryService implements LapPageQueryUseCase {

    private final LapPageQueryPort lapPageQueryPort;
    private final MotorcycleByIdQueryPort motorcycleByIdQueryPort;
    private final BikerByIdQueryPort bikerByIdQueryPort;
    private final CircuitByIdQueryPort circuitByIdQueryPort;

    @Override
    public ResultView<LapView> findAll(ResourceFilter filter) {
        var lapAggregateResultView = lapPageQueryPort.findAll(filter);
        return ResultView.<LapView>builder()
                .withPageInfo(lapAggregateResultView.pageInfo())
                .withResults(lapAggregateResultView.results().stream()
                        .map(lapAggregate -> LapView.toView(lapAggregate).toBuilder()
                                .withCircuit(CircuitSimplifiedView.toView(circuitByIdQueryPort.findById(lapAggregate.circuitId().toString())))
                                .withBiker(BikerView.toView(bikerByIdQueryPort.getById(lapAggregate.bikerId().toString())))
                                .withMotorcycle(MotorcycleView.toView(motorcycleByIdQueryPort
                                        .execute(lapAggregate.motorcycleId().toString())))
                                .build())
                        .collect(Collectors.toList()))
                .build();
    }
}