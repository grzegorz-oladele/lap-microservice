package pl.grzegorz.lapapplication.services.biker;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.lapapplication.ports.in.biker.command.BikerCreateCommandUseCase;
import pl.grzegorz.lapapplication.ports.out.biker.command.BikerCreateCommandPort;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;

@RequiredArgsConstructor
public class BikerCreateCommandService implements BikerCreateCommandUseCase {

    private final BikerCreateCommandPort bikerCreateCommandPort;

    @Override
    public void create(BikerCreateCommand createCommand) {
        var bikerAggregate = BikerAggregate.create(createCommand.toCreateData());
        bikerCreateCommandPort.create(bikerAggregate);
    }
}