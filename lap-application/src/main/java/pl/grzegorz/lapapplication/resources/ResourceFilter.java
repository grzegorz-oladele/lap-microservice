package pl.grzegorz.lapapplication.resources;

public record ResourceFilter(
        Integer page,
        Integer size
) {

    private static final int DEFAULT_PAGE_SIZE = 10;

    public static ResourceFilter empty() {
        return new ResourceFilter(0, DEFAULT_PAGE_SIZE);
    }

}
