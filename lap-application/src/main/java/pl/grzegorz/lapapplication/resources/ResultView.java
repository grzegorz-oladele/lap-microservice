package pl.grzegorz.lapapplication.resources;

import lombok.Builder;

import java.util.List;

@Builder(setterPrefix = "with")
public record ResultView<T>(
        PageInfo pageInfo,
        List<T> results
) {
}