package pl.grzegorz.lapadapters.out.persistence.circuit.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.lapdomain.aggregates.CircuitAggregate;

import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;
import static pl.grzegorz.lapadapters.out.Fixtures.*;

@ExtendWith(MockitoExtension.class)
class CircuitByIdQueryAdapterTest {

    @InjectMocks
    private CircuitByIdQueryAdapter circuitByIdQueryAdapter;
    @Mock
    private CircuitViewRepository circuitViewRepository;
    @Mock
    private CircuitViewMapper circuitViewMapper;

    private CircuitAggregate circuitAggregate;
    private CircuitViewEntity circuitViewEntity;

    @BeforeEach
    void setup() {
        circuitAggregate = circuitAggregate();
        circuitViewEntity = circuitViewEntity();
    }

    @Test
    void shouldReturnCircuitAggregateById() {
//        given
        var circuitId = CIRCUIT_ID.toString();
        when(circuitViewRepository.findById(UUID.fromString(circuitId))).thenReturn(Optional.of(circuitViewEntity));
        when(circuitViewMapper.toDomain(circuitViewEntity)).thenReturn(circuitAggregate);
//        when
        var circuitAggregate = circuitByIdQueryAdapter.findById(circuitId);
//        then
        assertAll(
                () -> assertThat(circuitViewEntity.id(), is(circuitAggregate.id())),
                () -> assertThat(circuitViewEntity.name(), is(circuitAggregate.name())),
                () -> assertThat(circuitViewEntity.description(), is(circuitAggregate.description())),
                () -> assertThat(circuitViewEntity.streetName(), is(circuitAggregate.streetName())),
                () -> assertThat(circuitViewEntity.streetNumber(), is(circuitAggregate.streetNumber())),
                () -> assertThat(circuitViewEntity.city(), is(circuitAggregate.city())),
                () -> assertThat(circuitViewEntity.length(), is(circuitAggregate.length())),
                () -> assertThat(circuitViewEntity.phoneNumber(), is(circuitAggregate.phoneNumber())),
                () -> assertThat(circuitViewEntity.email(), is(circuitAggregate.email()))
        );
    }
}