package pl.grzegorz.lapadapters.out.persistence.motorcycle.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.grzegorz.lapadapters.out.Fixtures.motorcycleAggregate;
import static pl.grzegorz.lapadapters.out.Fixtures.motorcycleEntity;

@ExtendWith(MockitoExtension.class)
class MotorcycleOwnerUpdateCommandAdapterTest {

    @InjectMocks
    private MotorcycleOwnerUpdateCommandAdapter motorcycleOwnerUpdateCommandAdapter;
    @Mock
    private MotorcycleRepository motorcycleRepository;
    @Mock
    private MotorcycleMapper motorcycleMapper;

    private MotorcycleAggregate motorcycleAggregate;
    private MotorcycleEntity motorcycleEntity;

    @BeforeEach
    void setup() {
        motorcycleAggregate = motorcycleAggregate();
        motorcycleEntity = motorcycleEntity();
    }

    @Test
    void shouldCallSaveOnMotorcycleRepositoryInterface() {
//        given
        when(motorcycleMapper.toEntity(motorcycleAggregate)).thenReturn(motorcycleEntity);
//        when
        motorcycleOwnerUpdateCommandAdapter.updateOwner(motorcycleAggregate);
//        then
        verify(motorcycleRepository).save(motorcycleEntity);
    }
}