package pl.grzegorz.lapadapters.out.persistence.motorcycle.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;

import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;
import static pl.grzegorz.lapadapters.out.Fixtures.*;

@ExtendWith(MockitoExtension.class)
class MotorcycleByIdQueryAdapterTest {

    @InjectMocks
    private MotorcycleByIdQueryAdapter motorcycleByIdQueryAdapter;
    @Mock
    private MotorcycleViewRepository motorcycleViewRepository;
    @Mock
    private MotorcycleViewMapper motorcycleViewMapper;

    private MotorcycleAggregate motorcycleAggregate;
    private MotorcycleViewEntity motorcycleViewEntity;

    @BeforeEach
    void setup() {
        motorcycleAggregate = motorcycleAggregate();
        motorcycleViewEntity = motorcycleViewEntity();
    }

    @Test
    void shouldReturnMotorcycleById() {
//        given
        var motorcycleId = MOTORCYCLE_ID.toString();
        var bikerId = BIKER_ID.toString();
        when(motorcycleViewRepository.findById(UUID.fromString(motorcycleId))).thenReturn(Optional.of(motorcycleViewEntity));
        when(motorcycleViewMapper.toDomain(motorcycleViewEntity)).thenReturn(motorcycleAggregate);
//        when
        var result = motorcycleByIdQueryAdapter.execute(motorcycleId);
//        then
        assertAll(
                () -> assertThat(result.id(), is(motorcycleViewEntity.id())),
                () -> assertThat(result.brand(), is(motorcycleViewEntity.brand())),
                () -> assertThat(result.model(), is(motorcycleViewEntity.model())),
                () -> assertThat(result.vintage(), is(motorcycleViewEntity.vintage())),
                () -> assertThat(result.horsePower(), is(motorcycleViewEntity.horsePower())),
                () -> assertThat(result.capacity(), is(motorcycleViewEntity.capacity())),
                () -> assertThat(result.motorcycleClass(), is(motorcycleViewEntity.motorcycleClass())),
                () -> assertThat(result.serialNumber(), is(motorcycleViewEntity.serialNumber()))
        );
    }
}