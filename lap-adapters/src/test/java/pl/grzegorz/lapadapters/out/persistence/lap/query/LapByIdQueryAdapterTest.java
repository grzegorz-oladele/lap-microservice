package pl.grzegorz.lapadapters.out.persistence.lap.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.lapadapters.out.persistence.lap.command.LapEntity;
import pl.grzegorz.lapadapters.out.persistence.lap.command.LapMapper;
import pl.grzegorz.lapadapters.out.persistence.lap.command.LapRepository;
import pl.grzegorz.lapdomain.aggregates.LapAggregate;
import pl.grzegorz.lapdomain.exception.BikerNotFoundException;

import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static pl.grzegorz.lapadapters.out.Fixtures.*;

@ExtendWith(MockitoExtension.class)
class LapByIdQueryAdapterTest {

    @InjectMocks
    private LapByIdQueryAdapter lapByIdQueryAdapter;
    @Mock
    private LapViewRepository lapRepository;
    @Mock
    private LapViewMapper lapMapper;
    private LapAggregate lapAggregate;
    private LapViewEntity lapViewEntity;

    @BeforeEach
    void setup() {
        lapAggregate = lapAggregate();
        lapViewEntity = lapViewEntity();
    }

    @Test
    void shouldReturnLapAggregateWhenLapWillBeExistsInTheDatabase() {
//        given
        var lapId = LAP_ID;
        var bikerId = BIKER_ID;
        when(lapRepository.findById(lapId)).thenReturn(Optional.of(lapViewEntity));
        when(lapMapper.toDomain(lapViewEntity)).thenReturn(lapAggregate);
//        when
        var result = lapByIdQueryAdapter.findById(lapId.toString(), bikerId.toString());
//        then
        assertAll(
                () -> assertThat(result.id(), is(lapId)),
                () -> assertThat(result.bikerId(), is(bikerId)),
                () -> assertThat(result.circuitId(), is(CIRCUIT_ID)),
                () -> assertThat(result.motorcycleId(), is(MOTORCYCLE_ID)),
                () -> assertThat(result.lapTime(), is(lapAggregate.lapTime()))
        );
    }

    @Test
    void shouldThrowBikerNotFoundExceptionWhenBikerWillBeNotExistsInDatabase() {
        //        given
        var lapId = lapAggregate.id().toString();
        var bikerId = UUID.randomUUID().toString();
//        when
        when(lapRepository.findById(LAP_ID)).thenReturn(Optional.of(lapViewEntity));
//        then
        assertThrows(BikerNotFoundException.class, () -> lapByIdQueryAdapter.findById(lapId, bikerId));
    }
}