package pl.grzegorz.lapadapters.out.persistence.biker.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.grzegorz.lapadapters.out.Fixtures.bikerAggregate;
import static pl.grzegorz.lapadapters.out.Fixtures.bikerEntity;

@ExtendWith(MockitoExtension.class)
class BikerEnableCommandAdapterTest {

    @InjectMocks
    private BikerEnableCommandAdapter bikerEnableCommandAdapter;
    @Mock
    private BikerRepository bikerRepository;
    @Mock
    private BikerMapper bikerMapper;

    private BikerAggregate bikerAggregate;
    private BikerEntity bikerEntity;

    @BeforeEach
    void setup() {
        bikerAggregate = bikerAggregate();
        bikerEntity = bikerEntity();
    }

    @Test
    void shouldCallSaveMethodOnBikerRepository() {
//        given
        when(bikerMapper.toEntity(bikerAggregate)).thenReturn(bikerEntity);
//        when
        bikerEnableCommandAdapter.enable(bikerAggregate);
//        then
        verify(bikerRepository).save(bikerEntity);
    }
}