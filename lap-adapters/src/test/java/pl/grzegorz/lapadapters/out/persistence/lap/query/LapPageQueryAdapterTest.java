package pl.grzegorz.lapadapters.out.persistence.lap.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import pl.grzegorz.lapadapters.helper.LapApplicationHelper;
import pl.grzegorz.lapapplication.resources.ResourceFilter;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;
import static pl.grzegorz.lapadapters.out.Fixtures.*;

@ExtendWith(MockitoExtension.class)
class LapPageQueryAdapterTest {

    @InjectMocks
    private LapPageQueryAdapter lapPageQueryAdapter;
    @Mock
    private LapViewRepository lapRepository;
    @Mock
    private LapApplicationHelper<LapViewEntity> lapApplicationHelper;

    private ResourceFilter filter;
    private Pageable pageRequest;
    private LapViewEntity lapViewEntity;
    private Page<LapViewEntity> lapPage;

    @BeforeEach
    void setup() {
        filter = new ResourceFilter(null, null);
        pageRequest = PageRequest.of(0, 10);
        lapViewEntity = lapViewEntity();
        lapPage = new PageImpl<>(Collections.singletonList(lapViewEntity));
    }

    @Test
    void shouldReturnResultViewWitListOfLaps() {
//        given
        var circuitId = CIRCUIT_ID.toString();
        var bikerId = BIKER_ID.toString();
        var motorcycleId = MOTORCYCLE_ID.toString();
        when(lapApplicationHelper.getPageRequest(filter)).thenReturn(pageRequest);
        when(lapRepository.findAll(pageRequest)).thenReturn(lapPage);
//        when
        var resultViewPage = lapPageQueryAdapter.findAll(filter);
//        then
        assertAll(
                () -> assertThat(resultViewPage.results().size(), is(1))
        );
    }
}