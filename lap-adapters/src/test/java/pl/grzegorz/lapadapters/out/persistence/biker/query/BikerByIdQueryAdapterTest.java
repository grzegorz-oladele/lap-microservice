package pl.grzegorz.lapadapters.out.persistence.biker.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;

import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;
import static pl.grzegorz.lapadapters.out.Fixtures.*;

@ExtendWith(MockitoExtension.class)
class BikerByIdQueryAdapterTest {

    @InjectMocks
    private BikerByIdQueryAdapter bikerByIdQueryAdapter;
    @Mock
    private BikerViewRepository bikerViewRepository;
    @Mock
    private BikerViewMapper bikerViewMapper;

    private BikerAggregate bikerAggregate;
    private BikerViewEntity bikerViewEntity;

    @BeforeEach
    void setup() {
        bikerAggregate = bikerAggregate();
        bikerViewEntity = bikerViewEntity();
    }

    @Test
    void shouldReturnBikerAggregateById() {
//        given
        var bikerId = BIKER_ID.toString();
        when(bikerViewRepository.findById(UUID.fromString(bikerId))).thenReturn(Optional.of(bikerViewEntity));
        when(bikerViewMapper.toDomain(bikerViewEntity)).thenReturn(bikerAggregate);
//        when
        var result = bikerByIdQueryAdapter.getById(bikerId);
//        then
        assertAll(
                () -> assertThat(result.id(), is(bikerViewEntity.id())),
                () -> assertThat(result.userName(), is(bikerViewEntity.userName())),
                () -> assertThat(result.firstName(), is(bikerViewEntity.firstName())),
                () -> assertThat(result.lastName(), is(bikerViewEntity.lastName())),
                () -> assertThat(result.email(), is(bikerViewEntity.email())),
                () -> assertThat(result.phoneNumber(), is(bikerViewEntity.phoneNumber())),
                () -> assertThat(result.dateOfBirth(), is(bikerViewEntity.dateOfBirth())),
                () -> assertThat(result.isActive(), is(bikerViewEntity.enable()))
        );
    }
}