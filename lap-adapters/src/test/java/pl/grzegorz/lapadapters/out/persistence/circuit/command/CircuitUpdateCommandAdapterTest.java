package pl.grzegorz.lapadapters.out.persistence.circuit.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.lapdomain.aggregates.CircuitAggregate;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.grzegorz.lapadapters.out.Fixtures.circuitAggregate;
import static pl.grzegorz.lapadapters.out.Fixtures.circuitEntity;

@ExtendWith(MockitoExtension.class)
class CircuitUpdateCommandAdapterTest {

    @InjectMocks
    private CircuitUpdateCommandAdapter circuitUpdateCommandAdapter;
    @Mock
    private CircuitRepository circuitRepository;
    @Mock
    private CircuitMapper circuitMapper;

    private CircuitEntity circuitEntity;
    private CircuitAggregate circuitAggregate;

    @BeforeEach
    void setup() {
        circuitEntity = circuitEntity();
        circuitAggregate = circuitAggregate();
    }

    @Test
    void shouldCallSaveMethodOnCircuitRepository() {
//        given
        when(circuitMapper.toEntity(circuitAggregate)).thenReturn(circuitEntity);
//        when
        circuitUpdateCommandAdapter.update(circuitAggregate);
//        then
        verify(circuitRepository).save(circuitEntity);
    }

}