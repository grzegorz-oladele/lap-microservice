package pl.grzegorz.lapadapters.out;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.grzegorz.lapadapters.in.amqp.motorcycle.dto.MotorcycleOwnerUpdateDto;
import pl.grzegorz.lapadapters.out.persistence.biker.command.BikerEntity;
import pl.grzegorz.lapadapters.out.persistence.biker.query.BikerViewEntity;
import pl.grzegorz.lapadapters.out.persistence.circuit.command.CircuitEntity;
import pl.grzegorz.lapadapters.out.persistence.circuit.query.CircuitViewEntity;
import pl.grzegorz.lapadapters.out.persistence.lap.command.LapEntity;
import pl.grzegorz.lapadapters.out.persistence.lap.query.LapViewEntity;
import pl.grzegorz.lapadapters.out.persistence.motorcycle.command.MotorcycleEntity;
import pl.grzegorz.lapadapters.out.persistence.motorcycle.query.MotorcycleViewEntity;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;
import pl.grzegorz.lapdomain.aggregates.CircuitAggregate;
import pl.grzegorz.lapdomain.aggregates.LapAggregate;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.lapdomain.data.motorcycle.MotorcycleClass;

import java.time.LocalDate;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Fixtures {

    public static final UUID LAP_ID = UUID.fromString("d983f9b2-485b-4f05-9cc3-1637993d7c3a");
    public static final UUID CIRCUIT_ID = UUID.fromString("98ba7243-999f-4350-810c-cf5c8b013baa");
    public static final UUID BIKER_ID = UUID.fromString("de7bb298-4d64-4f43-98cb-b340cc0feddb");
    private static final UUID SECOND_BIKER_ID = UUID.fromString("db23b58e-31e0-4c8b-b869-8d1bb7919dca");
    public static final UUID MOTORCYCLE_ID = UUID.fromString("9773aae6-ad7e-492d-b32c-5e50dd1e313b");

    public static LapAggregate lapAggregate() {
        return LapAggregate.builder()
                .withId(LAP_ID)
                .withBikerId(BIKER_ID)
                .withMotorcycleId(MOTORCYCLE_ID)
                .withCircuitId(CIRCUIT_ID)
                .withLapDate(LocalDate.now())
                .withLapTime("1:33.909")
                .build();
    }

    public static LapEntity lapEntity() {
        return LapEntity.builder()
                .withId(LAP_ID)
                .withBikerId(BIKER_ID)
                .withMotorcycleId(MOTORCYCLE_ID)
                .withCircuitId(CIRCUIT_ID)
                .withLapDate(LocalDate.now())
                .withLapTime("1:33.909")
                .build();
    }

    public static LapViewEntity lapViewEntity() {
        return LapViewEntity.builder()
                .withId(LAP_ID)
                .withBikerId(BIKER_ID)
                .withMotorcycleId(MOTORCYCLE_ID)
                .withCircuitId(CIRCUIT_ID)
                .withLapDate(LocalDate.now())
                .withLapTime("1:33.909")
                .build();
    }

    public static CircuitEntity circuitEntity() {
        return CircuitEntity.builder()
                .withId(CIRCUIT_ID)
                .withName("Tor Poznań")
                .withDescription("The biggest circuit in Poland")
                .withStreetName("Wyścigowa")
                .withStreetNumber("3")
                .withCity("Poznań")
                .withPostalCode("62-081")
                .withLength(4.087)
                .withEmail("tor@poznan.pl")
                .withPhoneNumber("123-456-789")
                .build();
    }

    public static CircuitViewEntity circuitViewEntity() {
        return CircuitViewEntity.builder()
                .withId(CIRCUIT_ID)
                .withName("Tor Poznań")
                .withDescription("The biggest circuit in Poland")
                .withStreetName("Wyścigowa")
                .withStreetNumber("3")
                .withCity("Poznań")
                .withPostalCode("62-081")
                .withLength(4.087)
                .withEmail("tor@poznan.pl")
                .withPhoneNumber("123-456-789")
                .build();
    }

    public static CircuitAggregate circuitAggregate() {
        return CircuitAggregate.builder()
                .withId(CIRCUIT_ID)
                .withName("Tor Poznań")
                .withDescription("The biggest circuit in Poland")
                .withStreetName("Wyścigowa")
                .withStreetNumber("3")
                .withCity("Poznań")
                .withPostalCode("62-081")
                .withLength(4.087)
                .withEmail("tor@poznan.pl")
                .withPhoneNumber("123-456-789")
                .build();
    }

    public static MotorcycleAggregate motorcycleAggregate() {
        return MotorcycleAggregate.builder()
                .withId(MOTORCYCLE_ID)
                .withBikerId(BIKER_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withVintage(2022)
                .withHorsePower(225)
                .withCapacity(1199)
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .withIsActive(Boolean.TRUE)
                .withSerialNumber("IUBERNCDJOSKL")
                .build();
    }

    public static MotorcycleEntity motorcycleEntity() {
        return MotorcycleEntity.builder()
                .withId(MOTORCYCLE_ID)
                .withBikerId(BIKER_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withVintage(2022)
                .withHorsePower(225)
                .withCapacity(1199)
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .withIsActive(Boolean.TRUE)
                .withSerialNumber("IUBERNCDJOSKL")
                .build();
    }

    public static MotorcycleViewEntity motorcycleViewEntity() {
        return MotorcycleViewEntity.builder()
                .withId(MOTORCYCLE_ID)
                .withBikerId(BIKER_ID)
                .withBrand("Ducati")
                .withModel("Panigale V4S")
                .withVintage(2022)
                .withHorsePower(225)
                .withCapacity(1199)
                .withMotorcycleClass(MotorcycleClass.SUPERSPORT)
                .withIsActive(Boolean.TRUE)
                .withSerialNumber("IUBERNCDJOSKL")
                .build();
    }

    public static BikerAggregate bikerAggregate() {
        return BikerAggregate.builder()
                .withId(BIKER_ID)
                .withUserName("tomasz_123")
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withDateOfBirth(LocalDate.of(1988, 4, 12))
                .withIsActive(Boolean.TRUE)
                .withEmail("tomasz@123.pl")
                .withPhoneNumber("123-456-789")
                .build();
    }

    public static BikerEntity bikerEntity() {
        return BikerEntity.builder()
                .withId(BIKER_ID)
                .withUserName("tomasz_123")
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withDateOfBirth(LocalDate.of(1988, 4, 12))
                .withEnable(Boolean.TRUE)
                .withEmail("tomasz@123.pl")
                .withPhoneNumber("123-456-789")
                .build();
    }

    public static BikerViewEntity bikerViewEntity() {
        return BikerViewEntity.builder()
                .withId(BIKER_ID)
                .withUserName("tomasz_123")
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withDateOfBirth(LocalDate.of(1988, 4, 12))
                .withEnable(Boolean.TRUE)
                .withEmail("tomasz@123.pl")
                .withPhoneNumber("123-456-789")
                .build();
    }

    public static MotorcycleOwnerUpdateDto motorcycleOwnerUpdateDto() {
        return MotorcycleOwnerUpdateDto.builder()
                .withMotorcycleId(MOTORCYCLE_ID)
                .withPreviousOwnerId(BIKER_ID)
                .withNewOwnerId(SECOND_BIKER_ID)
                .build();
    }
}