package pl.grzegorz.lapadapters.out.persistence.lap.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.lapdomain.aggregates.LapAggregate;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.grzegorz.lapadapters.out.Fixtures.lapAggregate;
import static pl.grzegorz.lapadapters.out.Fixtures.lapEntity;

@ExtendWith(MockitoExtension.class)
class LapCreateCommandAdapterTest {

    @InjectMocks
    private LapCreateCommandAdapter lapCreateCommandAdapter;
    @Mock
    private LapRepository lapRepository;
    @Mock
    private LapMapper lapMapper;

    private LapAggregate lapAggregate;
    private LapEntity lapEntity;

    @BeforeEach
    void setup() {
        lapAggregate = lapAggregate();
        lapEntity = lapEntity();
    }

    @Test
    void shouldCallSaveMethodAndPublishAllMethodWhenBikerAndMotorcycleWillBeExistsInTheDatabase() {
//        given
        when(lapMapper.toEntity(lapAggregate)).thenReturn(lapEntity);
//        when
        lapCreateCommandAdapter.create(lapAggregate);
//        then
        verify(lapRepository).save(lapEntity);
    }
}