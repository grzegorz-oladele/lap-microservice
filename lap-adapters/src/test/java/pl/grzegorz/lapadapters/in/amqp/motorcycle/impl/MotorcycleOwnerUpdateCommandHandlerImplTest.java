package pl.grzegorz.lapadapters.in.amqp.motorcycle.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.lapadapters.in.amqp.motorcycle.dto.MotorcycleOwnerUpdateDto;
import pl.grzegorz.lapapplication.ports.in.motorcycle.command.MotorcycleOwnerUpdateCommandUseCase;

import static org.mockito.Mockito.verify;
import static pl.grzegorz.lapadapters.out.Fixtures.motorcycleOwnerUpdateDto;

@ExtendWith(MockitoExtension.class)
class MotorcycleOwnerUpdateCommandHandlerImplTest {

    @InjectMocks
    private MotorcycleOwnerUpdateCommandHandlerImpl motorcycleOwnerUpdateCommandHandler;
    @Mock
    private MotorcycleOwnerUpdateCommandUseCase motorcycleOwnerUpdateCommandUseCase;

    private MotorcycleOwnerUpdateDto motorcycleOwnerUpdateDto;

    @BeforeEach
    void setup() {
        motorcycleOwnerUpdateDto = motorcycleOwnerUpdateDto();
    }

    @Test
    void shouldCallUpdateOwnerOnMotorcycleOwnerUpdateCommandUseCaseInterface() {
//        given
//        when
        motorcycleOwnerUpdateCommandHandler.handle(motorcycleOwnerUpdateDto);
//        then
        verify(motorcycleOwnerUpdateCommandUseCase).updateOwner(motorcycleOwnerUpdateDto.motorcycleOwnerUpdateCommand());
    }
}