package pl.grzegorz.lapadapters.out.persistence.circuit.query;

import org.springframework.stereotype.Component;
import pl.grzegorz.lapadapters.out.persistence.circuit.command.CircuitEntity;
import pl.grzegorz.lapdomain.aggregates.CircuitAggregate;

@Component
public class CircuitViewMapper {

    public CircuitAggregate toDomain(CircuitViewEntity circuitEntity){
        return CircuitAggregate.builder()
                .withId(circuitEntity.id())
                .withName(circuitEntity.name())
                .withDescription(circuitEntity.description())
                .withCity(circuitEntity.city())
                .withPostalCode(circuitEntity.postalCode())
                .withStreetName(circuitEntity.streetName())
                .withStreetNumber(circuitEntity.streetNumber())
                .withLength(circuitEntity.length())
                .withEmail(circuitEntity.email())
                .withPhoneNumber(circuitEntity.phoneNumber())
                .withCreatedAt(circuitEntity.createdAt())
                .withModifiedAt(circuitEntity.modifiedAt())
                .build();
    }
}
