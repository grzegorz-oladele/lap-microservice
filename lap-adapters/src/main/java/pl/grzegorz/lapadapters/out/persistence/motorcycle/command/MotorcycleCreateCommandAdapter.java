package pl.grzegorz.lapadapters.out.persistence.motorcycle.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapapplication.ports.out.motorcycle.command.MotorcycleCreateCommandPort;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleCreateCommandAdapter implements MotorcycleCreateCommandPort {

    private final MotorcycleRepository motorcycleRepository;
    private final MotorcycleMapper motorcycleMapper;

    @Override
    public void create(MotorcycleAggregate motorcycleAggregate) {
        var motorcycleEntity = motorcycleMapper.toEntity(motorcycleAggregate);
        motorcycleRepository.save(motorcycleEntity);
        log.info("Create motorcycle using id -> [{}] for biker using id -> [{}]", motorcycleAggregate.id(),
                motorcycleAggregate.bikerId());
    }
}