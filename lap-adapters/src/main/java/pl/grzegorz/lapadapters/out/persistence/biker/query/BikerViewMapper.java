package pl.grzegorz.lapadapters.out.persistence.biker.query;

import org.springframework.stereotype.Component;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;

@Component
public class BikerViewMapper {

    public BikerAggregate toDomain(BikerViewEntity biker) {
        return BikerAggregate.builder()
                .withId(biker.id())
                .withUserName(biker.userName())
                .withFirstName(biker.firstName())
                .withLastName(biker.lastName())
                .withEmail(biker.email())
                .withIsActive(biker.enable())
                .withPhoneNumber(biker.phoneNumber())
                .withDateOfBirth(biker.dateOfBirth())
                .withCreatedAt(biker.createdAt())
                .withModifiedAt(biker.modifiedAt())
                .build();
    }
}
