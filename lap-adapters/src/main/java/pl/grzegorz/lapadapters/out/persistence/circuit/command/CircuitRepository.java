package pl.grzegorz.lapadapters.out.persistence.circuit.command;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CircuitRepository extends JpaRepository<CircuitEntity, UUID> {
}