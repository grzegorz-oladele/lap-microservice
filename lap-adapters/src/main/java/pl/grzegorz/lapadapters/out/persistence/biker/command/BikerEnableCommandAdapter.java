package pl.grzegorz.lapadapters.out.persistence.biker.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapapplication.ports.out.biker.command.BikerEnableCommandPort;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class BikerEnableCommandAdapter implements BikerEnableCommandPort {

    private final BikerRepository bikerRepository;
    private final BikerMapper bikerMapper;

    @Override
    public void enable(BikerAggregate bikerAggregate) {
        bikerAggregate.enable();
        bikerRepository.save(bikerMapper.toEntity(bikerAggregate));
        log.info("Enable biker using id -> [{}]", bikerAggregate.id());
    }
}