package pl.grzegorz.lapadapters.out.persistence.biker.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapapplication.ports.out.biker.command.BikerCreateCommandPort;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class BikerCreateCommandAdapter implements BikerCreateCommandPort {

    private final BikerRepository bikerRepository;
    private final BikerMapper bikerMapper;

    @Override
    public void create(BikerAggregate bikerAggregate) {
        var bikerEntity = bikerMapper.toEntity(bikerAggregate);
        bikerRepository.save(bikerEntity);
        log.info("Create new biker using id -> [{}]", bikerEntity.id());
    }
}