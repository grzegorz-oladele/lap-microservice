package pl.grzegorz.lapadapters.out.persistence.lap.command;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "laps")
@Getter
@Builder(setterPrefix = "with", toBuilder = true)
@Accessors(fluent = true)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class LapEntity {

    @Id
    @Column(nullable = false, unique = true)
    private UUID id;
    private String lapTime;
    private LocalDate lapDate;
    private UUID circuitId;
    private UUID motorcycleId;
    private UUID bikerId;
    private LocalDateTime createdAt;
    private LocalDateTime modifiedAt;
}