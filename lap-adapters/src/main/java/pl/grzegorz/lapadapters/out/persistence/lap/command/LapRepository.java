package pl.grzegorz.lapadapters.out.persistence.lap.command;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface LapRepository extends JpaRepository<LapEntity, UUID> {
}