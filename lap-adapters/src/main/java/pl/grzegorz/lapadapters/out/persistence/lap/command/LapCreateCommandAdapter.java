package pl.grzegorz.lapadapters.out.persistence.lap.command;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapapplication.ports.out.lap.command.LapCreateCommandPort;
import pl.grzegorz.lapdomain.aggregates.LapAggregate;

import java.util.UUID;

@Service
@RequiredArgsConstructor
class LapCreateCommandAdapter implements LapCreateCommandPort {

    private final LapRepository lapRepository;
    private final LapMapper lapMapper;

    @Override
    public UUID create(final LapAggregate lapAggregate) {
        var lapEntity = lapMapper.toEntity(lapAggregate);
        lapRepository.save(lapEntity);
        return lapEntity.id();
    }
}