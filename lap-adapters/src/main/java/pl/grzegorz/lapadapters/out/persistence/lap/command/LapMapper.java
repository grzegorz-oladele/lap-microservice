package pl.grzegorz.lapadapters.out.persistence.lap.command;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.grzegorz.lapdomain.aggregates.LapAggregate;

@Component
@RequiredArgsConstructor
public class LapMapper {

    public LapEntity toEntity(LapAggregate aggregate) {
        return LapEntity.builder()
                .withId(aggregate.id())
                .withLapTime(aggregate.lapTime())
                .withLapDate(aggregate.lapDate())
                .withCircuitId(aggregate.circuitId())
                .withBikerId(aggregate.bikerId())
                .withMotorcycleId(aggregate.motorcycleId())
                .withModifiedAt(aggregate.modifiedAt())
                .withCreatedAt(aggregate.createdAt())
                .build();
    }
}