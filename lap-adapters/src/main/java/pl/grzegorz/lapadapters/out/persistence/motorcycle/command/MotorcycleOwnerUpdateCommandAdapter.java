package pl.grzegorz.lapadapters.out.persistence.motorcycle.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapapplication.ports.out.motorcycle.command.MotorcycleOwnerUpdateCommandPort;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleOwnerUpdateCommandAdapter implements MotorcycleOwnerUpdateCommandPort {

    private final MotorcycleRepository motorcycleRepository;
    private final MotorcycleMapper motorcycleMapper;

    @Override
    public void updateOwner(MotorcycleAggregate motorcycleAggregate) {
        var motorcycleEntity = motorcycleMapper.toEntity(motorcycleAggregate);
        motorcycleRepository.save(motorcycleEntity);
        log.info("Update owner of motorcycle. New owner id -> [{}]", motorcycleAggregate.bikerId());
    }
}