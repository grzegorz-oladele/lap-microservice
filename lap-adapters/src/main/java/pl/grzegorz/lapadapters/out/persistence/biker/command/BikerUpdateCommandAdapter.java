package pl.grzegorz.lapadapters.out.persistence.biker.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapapplication.ports.out.biker.command.BikerUpdateCommandPort;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class BikerUpdateCommandAdapter implements BikerUpdateCommandPort {

    private final BikerRepository bikerRepository;
    private final BikerMapper bikerMapper;

    @Override
    public void update(BikerAggregate bikerAggregate) {
        var updatedBiker = bikerMapper.toEntity(bikerAggregate);
        bikerRepository.save(updatedBiker);
        log.info("Update biker using id -> [{}]", updatedBiker.id());
    }
}