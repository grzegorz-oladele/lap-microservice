package pl.grzegorz.lapadapters.out.persistence.biker.query;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapadapters.out.persistence.biker.command.BikerMapper;
import pl.grzegorz.lapadapters.out.persistence.biker.command.BikerRepository;
import pl.grzegorz.lapapplication.ports.out.biker.query.BikerByIdQueryPort;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;
import pl.grzegorz.lapdomain.exception.BikerNotFoundException;
import pl.grzegorz.lapdomain.exception.messages.ExceptionMessage;

import java.util.UUID;

@Service
@RequiredArgsConstructor
class BikerByIdQueryAdapter implements BikerByIdQueryPort {

    private final BikerViewRepository bikerRepository;
    private final BikerViewMapper bikerMapper;

    @Override
    public BikerAggregate getById(String bikerId) {
        return bikerRepository.findById(UUID.fromString(bikerId))
                .map(bikerMapper::toDomain)
                .orElseThrow(() -> new BikerNotFoundException(
                        String.format(ExceptionMessage.BIKER_NOT_FOUND_EXCEPTION_MESSAGE.getMessage(), bikerId)));
    }
}