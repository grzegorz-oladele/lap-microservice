package pl.grzegorz.lapadapters.out.persistence.motorcycle.query;

import org.springframework.stereotype.Component;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;

@Component
public class MotorcycleViewMapper {

    public MotorcycleAggregate toDomain(MotorcycleViewEntity motorcycleEntity) {
        return MotorcycleAggregate.builder()
                .withId(motorcycleEntity.id())
                .withBikerId(motorcycleEntity.bikerId())
                .withBrand(motorcycleEntity.brand())
                .withModel(motorcycleEntity.model())
                .withCapacity(motorcycleEntity.capacity())
                .withHorsePower(motorcycleEntity.horsePower())
                .withVintage(motorcycleEntity.vintage())
                .withSerialNumber(motorcycleEntity.serialNumber())
                .withMotorcycleClass(motorcycleEntity.motorcycleClass())
                .withIsActive(motorcycleEntity.isActive())
                .withCreatedAt(motorcycleEntity.createdAt())
                .withModifiedAt(motorcycleEntity.modifiedAt())
                .build();
    }
}