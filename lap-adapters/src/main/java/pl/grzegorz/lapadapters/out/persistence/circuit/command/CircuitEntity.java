package pl.grzegorz.lapadapters.out.persistence.circuit.command;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "circuits")
@Getter
@Builder(setterPrefix = "with")
@Accessors(fluent = true)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class CircuitEntity {

    @Id
    @Column(nullable = false, unique = true)
    private UUID id;
    private String name;
    private String description;
    private String city;
    private String postalCode;
    private String streetName;
    private String streetNumber;
    private Double length;
    private String email;
    private String phoneNumber;
    private LocalDateTime createdAt;
    private LocalDateTime modifiedAt;
}