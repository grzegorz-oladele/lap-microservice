package pl.grzegorz.lapadapters.out.persistence.motorcycle.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapapplication.ports.out.motorcycle.command.MotorcycleMapEngineCommandPort;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleMapEngineCommandAdapter implements MotorcycleMapEngineCommandPort {

    private final MotorcycleMapper motorcycleMapper;
    private final MotorcycleRepository motorcycleRepository;

    @Override
    public void map(MotorcycleAggregate motorcycleAggregate) {
        var motorcycleEntity = motorcycleMapper.toEntity(motorcycleAggregate);
        motorcycleRepository.save(motorcycleEntity);
        log.info("Updating motorcycle using id -> [{}] after engine mapping", motorcycleAggregate.id());
    }
}
