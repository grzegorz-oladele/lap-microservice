package pl.grzegorz.lapadapters.out.persistence.motorcycle.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapapplication.ports.out.motorcycle.command.MotorcycleSetUnactiveCommandPort;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleSetUnactiveCommandAdapter implements MotorcycleSetUnactiveCommandPort {

    private final MotorcycleRepository motorcycleRepository;
    private final MotorcycleMapper motorcycleMapper;

    @Override
    public void execute(MotorcycleAggregate motorcycleAggregate) {
        motorcycleAggregate.setUnActive();
        var motorcycleEntity = motorcycleMapper.toEntity(motorcycleAggregate);
        motorcycleRepository.save(motorcycleEntity);
        log.info("Motorcycle using id -> [{}] is already unactive", motorcycleEntity.id());
    }
}