package pl.grzegorz.lapadapters.out.persistence.circuit.command;

import org.springframework.stereotype.Component;
import pl.grzegorz.lapdomain.aggregates.CircuitAggregate;

@Component
public class CircuitMapper {

    public CircuitEntity toEntity(CircuitAggregate aggregate) {
        return CircuitEntity.builder()
                .withId(aggregate.id())
                .withName(aggregate.name())
                .withDescription(aggregate.description())
                .withCity(aggregate.city())
                .withPostalCode(aggregate.postalCode())
                .withStreetName(aggregate.streetName())
                .withStreetNumber(aggregate.streetNumber())
                .withLength(aggregate.length())
                .withEmail(aggregate.email())
                .withPhoneNumber(aggregate.phoneNumber())
                .withCreatedAt(aggregate.createdAt())
                .withModifiedAt(aggregate.modifiedAt())
                .build();
    }
}