package pl.grzegorz.lapadapters.out.persistence.circuit.query;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CircuitViewRepository extends JpaRepository<CircuitViewEntity, UUID> {
}
