package pl.grzegorz.lapadapters.out.persistence.lap.query;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapadapters.helper.LapApplicationHelper;
import pl.grzegorz.lapapplication.ports.out.lap.query.LapPageQueryPort;
import pl.grzegorz.lapapplication.resources.ResourceFilter;
import pl.grzegorz.lapapplication.resources.ResultView;
import pl.grzegorz.lapdomain.aggregates.LapAggregate;

@Service
@RequiredArgsConstructor
class LapPageQueryAdapter implements LapPageQueryPort {

    private final LapViewRepository lapRepository;
    private final LapApplicationHelper<LapViewEntity> lapApplicationHelper;

    @Override
    public ResultView<LapAggregate> findAll(ResourceFilter filter) {
        Pageable pageRequest = lapApplicationHelper.getPageRequest(filter);
        Page<LapViewEntity> lapPage = lapRepository.findAll(pageRequest);
        return ResultView.<LapAggregate>builder()
                .withPageInfo(lapApplicationHelper.toPageInfo(pageRequest, lapPage))
                .withResults(lapPage.getContent()
                        .stream()
                        .map(lapEntity -> LapAggregate.builder()
                                .withId(lapEntity.id())
                                .withLapDate(lapEntity.lapDate())
                                .withLapTime(lapEntity.lapTime())
                                .withBikerId(lapEntity.bikerId())
                                .withMotorcycleId(lapEntity.motorcycleId())
                                .withCircuitId(lapEntity.circuitId())
                                .withModifiedAt(lapEntity.modifiedAt())
                                .withCreatedAt(lapEntity.createdAt())
                                .build())
                        .toList()
                )
                .build();
    }
}