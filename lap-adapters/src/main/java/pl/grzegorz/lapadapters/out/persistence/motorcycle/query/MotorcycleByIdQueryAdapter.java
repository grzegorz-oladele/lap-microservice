package pl.grzegorz.lapadapters.out.persistence.motorcycle.query;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapapplication.ports.out.motorcycle.query.MotorcycleByIdQueryPort;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;
import pl.grzegorz.lapdomain.exception.MotorcycleNotFoundException;
import pl.grzegorz.lapdomain.exception.messages.ExceptionMessage;

import java.util.UUID;

@Service
@RequiredArgsConstructor
class MotorcycleByIdQueryAdapter implements MotorcycleByIdQueryPort {

    private final MotorcycleViewRepository motorcycleViewRepository;
    private final MotorcycleViewMapper motorcycleViewMapper;

    @Override
    public MotorcycleAggregate execute(String motorcycleId) {
        return motorcycleViewRepository.findById(UUID.fromString(motorcycleId))
                .map(this::mapToMotorcycleAggregate)
                .orElseThrow(() -> new MotorcycleNotFoundException(
                        String.format(ExceptionMessage.MOTORCYCLE_NOT_FOUND_EXCEPTION_MESSAGE.getMessage(), motorcycleId)));
    }

    private MotorcycleAggregate mapToMotorcycleAggregate(MotorcycleViewEntity motorcycleViewEntity) {
        return motorcycleViewMapper.toDomain(motorcycleViewEntity);
    }
}