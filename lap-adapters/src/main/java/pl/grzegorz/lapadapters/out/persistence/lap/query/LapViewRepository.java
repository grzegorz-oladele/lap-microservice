package pl.grzegorz.lapadapters.out.persistence.lap.query;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface LapViewRepository extends JpaRepository<LapViewEntity, UUID> {
}