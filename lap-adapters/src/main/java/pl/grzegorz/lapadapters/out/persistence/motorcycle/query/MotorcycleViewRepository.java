package pl.grzegorz.lapadapters.out.persistence.motorcycle.query;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
interface MotorcycleViewRepository extends JpaRepository<MotorcycleViewEntity, UUID> {
}