package pl.grzegorz.lapadapters.out.persistence.circuit.query;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapapplication.ports.out.circuit.query.CircuitByIdQueryPort;
import pl.grzegorz.lapdomain.aggregates.CircuitAggregate;
import pl.grzegorz.lapdomain.exception.CircuitNotFoundException;
import pl.grzegorz.lapdomain.exception.messages.ExceptionMessage;

import java.util.UUID;

@Service
@RequiredArgsConstructor
class CircuitByIdQueryAdapter implements CircuitByIdQueryPort {

    private final CircuitViewRepository circuitRepository;
    private final CircuitViewMapper circuitMapper;

    @Override
    public CircuitAggregate findById(String circuitId) {
        return circuitRepository.findById(UUID.fromString(circuitId))
                .map(circuitMapper::toDomain)
                .orElseThrow(() -> new CircuitNotFoundException(
                        String.format(ExceptionMessage.CIRCUIT_NOT_FOUND_MESSAGE.getMessage(), circuitId)));
    }
}