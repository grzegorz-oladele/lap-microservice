package pl.grzegorz.lapadapters.out.persistence.lap.query;

import org.springframework.stereotype.Component;
import pl.grzegorz.lapdomain.aggregates.LapAggregate;

@Component
public class LapViewMapper {

    public LapAggregate toDomain(LapViewEntity lap) {
        return LapAggregate.builder()
                .withId(lap.id())
                .withLapTime(lap.lapTime())
                .withLapDate(lap.lapDate())
                .withBikerId(lap.bikerId())
                .withMotorcycleId(lap.motorcycleId())
                .withCircuitId(lap.circuitId())
                .withModifiedAt(lap.modifiedAt())
                .withCreatedAt(lap.createdAt())
                .build();
    }
}