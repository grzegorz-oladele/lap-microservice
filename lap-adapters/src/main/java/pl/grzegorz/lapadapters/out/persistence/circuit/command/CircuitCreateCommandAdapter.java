package pl.grzegorz.lapadapters.out.persistence.circuit.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapapplication.ports.out.circuit.command.CircuitCreateCommandPort;
import pl.grzegorz.lapdomain.aggregates.CircuitAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class CircuitCreateCommandAdapter implements CircuitCreateCommandPort {

    private final CircuitRepository circuitRepository;
    private final CircuitMapper circuitMapper;

    @Override
    public void create(CircuitAggregate circuitAggregate) {
        var circuitEntity = circuitMapper.toEntity(circuitAggregate);
        circuitRepository.save(circuitEntity);
        log.info("Create circuit using id -> [{}]", circuitEntity.id());
    }
}