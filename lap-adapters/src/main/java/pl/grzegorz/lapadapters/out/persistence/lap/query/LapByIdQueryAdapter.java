package pl.grzegorz.lapadapters.out.persistence.lap.query;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapapplication.ports.out.lap.query.LapByIdQueryPort;
import pl.grzegorz.lapdomain.aggregates.LapAggregate;
import pl.grzegorz.lapdomain.exception.BikerNotFoundException;
import pl.grzegorz.lapdomain.exception.LapNotFoundException;
import pl.grzegorz.lapdomain.exception.messages.ExceptionMessage;

import java.util.UUID;

@Service
@RequiredArgsConstructor
class LapByIdQueryAdapter implements LapByIdQueryPort {

    private final LapViewRepository lapRepository;
    private final LapViewMapper lapMapper;

    @Override
    public LapAggregate findById(String lapId, String bikerId) {
        return lapRepository.findById(UUID.fromString(lapId))
                .map(lap -> {
                    if (!lap.bikerId().toString().equals(bikerId)) {
                        throw new BikerNotFoundException(String.format(
                                ExceptionMessage.BIKER_NOT_FOUND_EXCEPTION_MESSAGE.getMessage(), bikerId));
                    }
                    return lapMapper.toDomain(lap);
                })
                .orElseThrow(() -> new LapNotFoundException(String.format(
                        ExceptionMessage.LAP_NOT_FOUND_EXCEPTION_MESSAGE.getMessage(), lapId)));
    }
}