package pl.grzegorz.lapadapters.out.persistence.motorcycle.command;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MotorcycleRepository extends JpaRepository<MotorcycleEntity, UUID> {
}