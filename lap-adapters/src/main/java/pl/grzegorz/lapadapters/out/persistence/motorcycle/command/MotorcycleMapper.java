package pl.grzegorz.lapadapters.out.persistence.motorcycle.command;

import org.springframework.stereotype.Component;
import pl.grzegorz.lapdomain.aggregates.MotorcycleAggregate;

@Component
public class MotorcycleMapper {

    public MotorcycleEntity toEntity(MotorcycleAggregate motorcycleAggregate) {
        return MotorcycleEntity.builder()
                .withId(motorcycleAggregate.id())
                .withBikerId(motorcycleAggregate.bikerId())
                .withBrand(motorcycleAggregate.brand())
                .withModel(motorcycleAggregate.model())
                .withCapacity(motorcycleAggregate.capacity())
                .withHorsePower(motorcycleAggregate.horsePower())
                .withVintage(motorcycleAggregate.vintage())
                .withMotorcycleClass(motorcycleAggregate.motorcycleClass())
                .withSerialNumber(motorcycleAggregate.serialNumber())
                .withIsActive(motorcycleAggregate.isActive())
                .withCreatedAt(motorcycleAggregate.createdAt())
                .withModifiedAt(motorcycleAggregate.modifiedAt())
                .build();
    }
}