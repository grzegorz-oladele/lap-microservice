package pl.grzegorz.lapadapters.out.persistence.biker.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapapplication.ports.out.biker.command.BikerDisableCommandPort;
import pl.grzegorz.lapdomain.aggregates.BikerAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class BikerDisableCommandAdapter implements BikerDisableCommandPort {

    private final BikerRepository bikerRepository;
    private final BikerMapper bikerMapper;

    @Override
    public void disable(BikerAggregate bikerAggregate) {
     bikerAggregate.disable();
     bikerRepository.save(bikerMapper.toEntity(bikerAggregate));
     log.info("Disable biker using id -> [{}]", bikerAggregate.id());
    }
}