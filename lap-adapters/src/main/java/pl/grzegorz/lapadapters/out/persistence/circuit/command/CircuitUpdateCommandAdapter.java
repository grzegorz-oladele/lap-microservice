package pl.grzegorz.lapadapters.out.persistence.circuit.command;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapapplication.ports.out.circuit.command.CircuitUpdateCommandPort;
import pl.grzegorz.lapdomain.aggregates.CircuitAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class CircuitUpdateCommandAdapter implements CircuitUpdateCommandPort {

    private final CircuitMapper circuitMapper;
    private final CircuitRepository circuitRepository;

    @Override
    public void update(CircuitAggregate circuitAggregate) {
        var circuitEntity = circuitMapper.toEntity(circuitAggregate);
        circuitRepository.save(circuitEntity);
        log.info("Update circuit using id -> [{}]", circuitEntity.id());
    }
}