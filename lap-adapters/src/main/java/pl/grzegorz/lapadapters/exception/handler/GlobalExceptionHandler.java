package pl.grzegorz.lapadapters.exception.handler;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import pl.grzegorz.lapdomain.exception.*;

@RestControllerAdvice
@RequiredArgsConstructor
class GlobalExceptionHandler {

    private final HttpServletRequest request;

    @ExceptionHandler({CircuitNotFoundException.class, HttpClientErrorException.NotFound.class,
            BikerNotFoundException.class, MotorcycleNotFoundException.class, LapNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ErrorResponse handleNotFoundException(RuntimeException exception) {
        return ErrorResponse.getErrorResponse(exception.getMessage(), getCurrentPath(), HttpStatus.NOT_FOUND.value());
    }

    @ExceptionHandler({LapCreateException.class, InvalidLapTimeFormatException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorResponse handleLapCreateException(Exception exception) {
        return ErrorResponse.getErrorResponse(exception.getMessage(), getCurrentPath(),
                HttpStatus.BAD_REQUEST.value());
    }

    @ExceptionHandler({MethodArgumentNotValidException.class, CircuitAlreadyExistsException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    ErrorResponse handleConflictUseCase(Exception exception) {
        if (exception instanceof CircuitAlreadyExistsException) {
            return ErrorResponse.getErrorResponse(exception.getMessage(), getCurrentPath(), HttpStatus.CONFLICT.value());
        }
        return ErrorResponse
                .getErrorResponse(getDefaultErrorMessage((MethodArgumentNotValidException) exception),
                        getCurrentPath(), HttpStatus.CONFLICT.value());
    }

    @ExceptionHandler({BikerPermissionException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    ErrorResponse handleBikerPermissionException(RuntimeException exception) {
        return ErrorResponse.getErrorResponse(exception.getMessage(), getCurrentPath(), HttpStatus.FORBIDDEN.value());
    }

    @ExceptionHandler({ConnectionRefusedException.class})
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    ErrorResponse handleConnectException(ConnectionRefusedException exception) {
        return ErrorResponse.getErrorResponse(exception.getMessage(), getCurrentPath(),
                HttpStatus.SERVICE_UNAVAILABLE.value());
    }

    @ExceptionHandler({BikerUnactiveException.class, MotorcycleUnactiveException.class})
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    ErrorResponse handleUnprocessableException(RuntimeException exception) {
        return ErrorResponse.getErrorResponse(exception.getMessage(), getCurrentPath(),
                HttpStatus.UNPROCESSABLE_ENTITY.value());
    }

    private String getDefaultErrorMessage(MethodArgumentNotValidException exception) {
        return exception.getAllErrors().get(0).getDefaultMessage();
    }

    private String getCurrentPath() {
        return request.getServletPath();
    }
}