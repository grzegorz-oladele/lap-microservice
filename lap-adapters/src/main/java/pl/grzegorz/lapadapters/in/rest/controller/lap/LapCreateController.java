package pl.grzegorz.lapadapters.in.rest.controller.lap;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.lapadapters.in.rest.api.lap.LapCreateApi;
import pl.grzegorz.lapadapters.in.rest.dto.LapCreateDto;
import pl.grzegorz.lapapplication.ports.in.lap.command.LapCreateCommandUseCase;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

@RestController
@RequiredArgsConstructor
class LapCreateController implements LapCreateApi {

    private final LapCreateCommandUseCase lapCreateCommandUseCase;

    @Override
    public ResponseEntity<UUID> createLap(String circuitId,
                                          String bikerId,
                                          String motorcycleId,
                                          LapCreateDto lapDto) throws ExecutionException, InterruptedException {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(lapCreateCommandUseCase.create(circuitId, bikerId, motorcycleId, lapDto.toCreateCommand())
                );
    }
}