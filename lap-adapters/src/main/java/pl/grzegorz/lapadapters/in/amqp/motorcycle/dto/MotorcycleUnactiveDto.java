package pl.grzegorz.lapadapters.in.amqp.motorcycle.dto;

import java.io.Serializable;
import java.util.UUID;

public record MotorcycleUnactiveDto(
        UUID id,
        UUID bikerId
) implements Serializable {
}