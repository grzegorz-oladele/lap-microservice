package pl.grzegorz.lapadapters.in.amqp.motorcycle.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapadapters.in.amqp.motorcycle.dto.MotorcycleCreateDto;
import pl.grzegorz.lapadapters.in.amqp.motorcycle.handler.MotorcycleCreateCommandHandler;
import pl.grzegorz.lapapplication.ports.in.motorcycle.command.MotorcycleCreateCommandUseCase;

@Service
@RequiredArgsConstructor
@Slf4j
class MotorcycleCreateCommandHandlerImpl implements MotorcycleCreateCommandHandler {

    private final MotorcycleCreateCommandUseCase motorcycleCreateCommandUseCase;

    @Override
    public void create(MotorcycleCreateDto motorcycleCreateDto) {
        motorcycleCreateCommandUseCase.create(motorcycleCreateDto.toCreateCommand());
    }
}