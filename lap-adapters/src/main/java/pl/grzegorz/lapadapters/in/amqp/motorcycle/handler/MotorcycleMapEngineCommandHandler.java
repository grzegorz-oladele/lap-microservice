package pl.grzegorz.lapadapters.in.amqp.motorcycle.handler;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.lapadapters.in.amqp.motorcycle.dto.MotorcycleMapEngineDto;

public interface MotorcycleMapEngineCommandHandler {

    @RabbitListener(queues = "${spring.motorcycle.map-engine.queue}")
    void handle(MotorcycleMapEngineDto motorcycleMapEngineDto);
}