package pl.grzegorz.lapadapters.in.amqp.motorcycle.dto;

import lombok.Builder;
import pl.grzegorz.lapapplication.ports.in.motorcycle.command.MotorcycleOwnerUpdateCommandUseCase.MotorcycleOwnerUpdateCommand;

import java.io.Serializable;
import java.util.UUID;

@Builder(setterPrefix = "with")
public record MotorcycleOwnerUpdateDto(
        UUID motorcycleId,
        UUID previousOwnerId,
        UUID newOwnerId
) implements Serializable {

    public MotorcycleOwnerUpdateCommand motorcycleOwnerUpdateCommand() {
        return MotorcycleOwnerUpdateCommand.builder()
                .withMotorcycleId(motorcycleId)
                .withPreviousOwnerId(previousOwnerId)
                .withNewOwnerId(newOwnerId)
                .build();
    }
}