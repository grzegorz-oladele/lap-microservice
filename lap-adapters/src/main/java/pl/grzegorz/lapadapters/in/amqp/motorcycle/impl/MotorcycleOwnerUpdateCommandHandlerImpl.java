package pl.grzegorz.lapadapters.in.amqp.motorcycle.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapadapters.in.amqp.motorcycle.dto.MotorcycleOwnerUpdateDto;
import pl.grzegorz.lapadapters.in.amqp.motorcycle.handler.MotorcycleOwnerUpdateCommandHandler;
import pl.grzegorz.lapapplication.ports.in.motorcycle.command.MotorcycleOwnerUpdateCommandUseCase;

@Service
@RequiredArgsConstructor
class MotorcycleOwnerUpdateCommandHandlerImpl implements MotorcycleOwnerUpdateCommandHandler {

    private final MotorcycleOwnerUpdateCommandUseCase motorcycleOwnerUpdateCommandUseCase;

    @Override
    public void handle(MotorcycleOwnerUpdateDto motorcycleOwnerUpdateDto) {
        motorcycleOwnerUpdateCommandUseCase.updateOwner(motorcycleOwnerUpdateDto.motorcycleOwnerUpdateCommand());
    }
}