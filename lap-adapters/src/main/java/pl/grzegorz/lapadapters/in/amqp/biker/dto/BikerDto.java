package pl.grzegorz.lapadapters.in.amqp.biker.dto;

import pl.grzegorz.lapapplication.ports.in.biker.command.BikerCreateCommandUseCase.BikerCreateCommand;

import java.io.Serializable;
import java.util.UUID;

public record BikerDto(
        UUID id,
        String username,
        String firstName,
        String lastName,
        String email,
        Boolean enable,
        String phoneNumber,
        String dateOfBirth
) implements Serializable {

    public BikerCreateCommand toCreateCommand() {
        return BikerCreateCommand.builder()
                .withId(id)
                .withUsername(username)
                .withFirstName(firstName)
                .withLastName(lastName)
                .withEmail(email)
                .withEnable(enable)
                .withPhoneNumber(phoneNumber)
                .withDateOfBirth(dateOfBirth)
                .build();
    }
}