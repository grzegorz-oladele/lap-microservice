package pl.grzegorz.lapadapters.in.rest.controller.lap;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.lapadapters.in.rest.api.lap.LapPageApi;
import pl.grzegorz.lapapplication.ports.in.lap.query.LapPageQueryUseCase;
import pl.grzegorz.lapapplication.ports.view.LapView;
import pl.grzegorz.lapapplication.resources.ResourceFilter;
import pl.grzegorz.lapapplication.resources.ResultView;

@RestController
@RequiredArgsConstructor
class LapPageController implements LapPageApi {

    private final LapPageQueryUseCase lapPageQueryUseCase;

    @Override
    public ResponseEntity<ResultView<LapView>> findAll(ResourceFilter filter) {
        return ResponseEntity.status(HttpStatus.OK).body(lapPageQueryUseCase.findAll(filter));
    }
}