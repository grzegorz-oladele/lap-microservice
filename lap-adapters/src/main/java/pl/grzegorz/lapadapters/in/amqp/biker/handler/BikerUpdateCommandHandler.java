package pl.grzegorz.lapadapters.in.amqp.biker.handler;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.lapadapters.in.amqp.biker.dto.BikerUpdateDto;

public interface BikerUpdateCommandHandler {

    @RabbitListener(queues = "${spring.biker.update.queue}")
    void update(BikerUpdateDto bikerUpdateDto);
}
