package pl.grzegorz.lapadapters.in.amqp.circuit.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapadapters.in.amqp.circuit.consumer.CircuitUpdateCommandHandler;
import pl.grzegorz.lapadapters.in.amqp.circuit.dto.CircuitUpdateDto;
import pl.grzegorz.lapapplication.ports.in.circuit.command.CircuitUpdateCommandUseCase;

@Service
@RequiredArgsConstructor
class CircuitUpdateCommandHandlerImpl implements CircuitUpdateCommandHandler {

    private final CircuitUpdateCommandUseCase circuitUpdateCommandUseCase;

    @Override
    public void update(CircuitUpdateDto circuitUpdateDto) {
        circuitUpdateCommandUseCase.update(circuitUpdateDto.toUpdateCommand());
    }
}