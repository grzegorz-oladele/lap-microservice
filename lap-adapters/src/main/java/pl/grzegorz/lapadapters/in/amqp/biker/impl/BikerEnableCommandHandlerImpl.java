package pl.grzegorz.lapadapters.in.amqp.biker.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapadapters.in.amqp.biker.handler.BikerEnableCommandHandler;
import pl.grzegorz.lapadapters.in.amqp.biker.dto.BikerEnableDto;
import pl.grzegorz.lapapplication.ports.in.biker.command.BikerEnableCommandUseCase;

@Service
@RequiredArgsConstructor
class BikerEnableCommandHandlerImpl implements BikerEnableCommandHandler {

    private final BikerEnableCommandUseCase bikerEnableCommandUseCase;

    @Override
    public void enable(BikerEnableDto bikerEnableDto) {
        bikerEnableCommandUseCase.enable(bikerEnableDto.id());
    }
}