package pl.grzegorz.lapadapters.in.rest.dto;

import jakarta.validation.constraints.NotBlank;
import pl.grzegorz.lapapplication.ports.in.lap.command.LapCreateCommandUseCase.LapCreateCommand;

import java.time.LocalDate;

public record LapCreateDto(
        @NotBlank(message = "Lap time must not be blank")
        String lapTime,
        @NotBlank(message = "Lap date must not be blank")
        String lapDate
) {

    public LapCreateCommand toCreateCommand() {
        return LapCreateCommand.builder()
                .withLapTime(lapTime)
                .withLapDate(LocalDate.parse(lapDate))
                .build();
    }
}