package pl.grzegorz.lapadapters.in.amqp.motorcycle.dto;

import pl.grzegorz.lapapplication.ports.in.motorcycle.command.MotorcycleCreateCommandUseCase.MotorcycleCreateCommand;
import pl.grzegorz.lapdomain.data.motorcycle.MotorcycleClass;

import java.io.Serializable;
import java.util.UUID;

public record MotorcycleCreateDto(
        UUID id,
        UUID bikerId,
        String brand,
        String model,
        Integer capacity,
        Integer horsePower,
        Integer vintage,
        String serialNumber,
        MotorcycleClass motorcycleClass
) implements Serializable {

    public MotorcycleCreateCommand toCreateCommand() {
        return MotorcycleCreateCommand.builder()
                .withId(id)
                .withBikerId(bikerId)
                .withBrand(brand)
                .withModel(model)
                .withCapacity(capacity)
                .withHorsePower(horsePower)
                .withVintage(vintage)
                .withSerialNumber(serialNumber)
                .withMotorcycleClass(motorcycleClass)
                .build();
    }
}