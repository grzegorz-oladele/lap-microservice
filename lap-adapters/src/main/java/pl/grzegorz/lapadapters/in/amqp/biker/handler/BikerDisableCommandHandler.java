package pl.grzegorz.lapadapters.in.amqp.biker.handler;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.lapadapters.in.amqp.biker.dto.BikerEnableDto;

public interface BikerDisableCommandHandler {

    @RabbitListener(queues = "${spring.biker.disable.queue}")
    void disable(BikerEnableDto bikerEnableDto);
}
