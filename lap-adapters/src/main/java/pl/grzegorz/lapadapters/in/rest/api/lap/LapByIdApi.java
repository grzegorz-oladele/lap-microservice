package pl.grzegorz.lapadapters.in.rest.api.lap;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.grzegorz.lapapplication.ports.view.LapView;

@RequestMapping("/laps")
public interface LapByIdApi {

    @GetMapping("/{lapId}/bikers/{bikerId}")
    ResponseEntity<LapView> findById(
            @PathVariable("lapId") String lapId,
            @PathVariable("bikerId") String bikerId
    );
}