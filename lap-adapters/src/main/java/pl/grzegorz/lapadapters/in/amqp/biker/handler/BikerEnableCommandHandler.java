package pl.grzegorz.lapadapters.in.amqp.biker.handler;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.lapadapters.in.amqp.biker.dto.BikerEnableDto;

public interface BikerEnableCommandHandler {

    @RabbitListener(queues = "${spring.biker.enable.queue}")
    void enable(BikerEnableDto bikerEnableDto);
}