package pl.grzegorz.lapadapters.in.amqp.motorcycle.handler;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.lapadapters.in.amqp.motorcycle.dto.MotorcycleCreateDto;

public interface MotorcycleCreateCommandHandler {

    @RabbitListener(queues = "${spring.motorcycle.create.queue}")
    void create(MotorcycleCreateDto motorcycleCreateDto);
}