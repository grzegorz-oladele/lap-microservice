package pl.grzegorz.lapadapters.in.amqp.motorcycle.handler;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.lapadapters.in.amqp.motorcycle.dto.MotorcycleOwnerUpdateDto;

public interface MotorcycleOwnerUpdateCommandHandler {

    @RabbitListener(queues = "${spring.motorcycle.update-owner.queue}")
    void handle(MotorcycleOwnerUpdateDto motorcycleOwnerUpdateDto);
}