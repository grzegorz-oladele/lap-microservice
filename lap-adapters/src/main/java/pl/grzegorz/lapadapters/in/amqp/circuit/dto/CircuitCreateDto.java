package pl.grzegorz.lapadapters.in.amqp.circuit.dto;

import pl.grzegorz.lapapplication.ports.in.circuit.command.CircuitCreateCommandUseCase.CircuitCreateCommand;

import java.io.Serializable;
import java.util.UUID;

public record CircuitCreateDto(
        UUID id,
        String name,
        String description,
        String city,
        String postalCode,
        String streetName,
        String streetNumber,
        double length,
        String email,
        String phoneNumber
) implements Serializable {

    public CircuitCreateCommand toCreateData() {
        return CircuitCreateCommand.builder()
                .withId(id)
                .withName(name)
                .withDescription(description)
                .withCity(city)
                .withPostalCode(postalCode)
                .withStreetName(streetName)
                .withStreetNumber(streetNumber)
                .withLength(length)
                .withEmail(email)
                .withPhoneNumber(phoneNumber)
                .build();
    }
}