package pl.grzegorz.lapadapters.in.amqp.circuit.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.lapadapters.in.amqp.circuit.dto.CircuitCreateDto;

public interface CircuitCreateCommandHandler {

    @RabbitListener(queues = "${spring.circuit.create.queue}")
    void create(CircuitCreateDto circuitCreateDto);
}