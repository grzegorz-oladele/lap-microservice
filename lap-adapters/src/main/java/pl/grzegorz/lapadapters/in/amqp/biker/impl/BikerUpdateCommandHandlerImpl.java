package pl.grzegorz.lapadapters.in.amqp.biker.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapadapters.in.amqp.biker.handler.BikerUpdateCommandHandler;
import pl.grzegorz.lapadapters.in.amqp.biker.dto.BikerUpdateDto;
import pl.grzegorz.lapapplication.ports.in.biker.command.BikerUpdateCommandUseCase;

@Service
@RequiredArgsConstructor
class BikerUpdateCommandHandlerImpl implements BikerUpdateCommandHandler {

    private final BikerUpdateCommandUseCase bikerUpdateCommandUseCase;

    @Override
    public void update(BikerUpdateDto bikerUpdateDto) {
        bikerUpdateCommandUseCase.update(bikerUpdateDto.bikerUpdateCommand());
    }
}