package pl.grzegorz.lapadapters.in.rest.controller.lap;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.lapadapters.in.rest.api.lap.LapByIdApi;
import pl.grzegorz.lapapplication.ports.in.lap.query.LapByIdQueryUseCase;
import pl.grzegorz.lapapplication.ports.view.LapView;

@RestController
@RequiredArgsConstructor
class LapByIdController implements LapByIdApi {

    private final LapByIdQueryUseCase lapByIdQueryUseCase;

    @Override
    public ResponseEntity<LapView> findById(String lapId, String bikerId) {
        return ResponseEntity.status(HttpStatus.OK).body(lapByIdQueryUseCase.findById(lapId, bikerId));
    }
}