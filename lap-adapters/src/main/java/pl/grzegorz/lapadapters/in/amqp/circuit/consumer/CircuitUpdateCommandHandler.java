package pl.grzegorz.lapadapters.in.amqp.circuit.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.lapadapters.in.amqp.circuit.dto.CircuitUpdateDto;

public interface CircuitUpdateCommandHandler {

    @RabbitListener(queues = "${spring.circuit.update.queue}")
    void update(CircuitUpdateDto circuitUpdateDto);
}