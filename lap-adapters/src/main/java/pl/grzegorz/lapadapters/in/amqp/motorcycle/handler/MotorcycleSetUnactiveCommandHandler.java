package pl.grzegorz.lapadapters.in.amqp.motorcycle.handler;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.lapadapters.in.amqp.motorcycle.dto.MotorcycleUnactiveDto;

public interface MotorcycleSetUnactiveCommandHandler {

    @RabbitListener(queues = "${spring.motorcycle.unactive.queue}")
    void handle(MotorcycleUnactiveDto motorcycleUnactiveDto);
}