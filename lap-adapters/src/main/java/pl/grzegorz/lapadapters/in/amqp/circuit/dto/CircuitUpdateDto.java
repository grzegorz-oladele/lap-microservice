package pl.grzegorz.lapadapters.in.amqp.circuit.dto;

import pl.grzegorz.lapapplication.ports.in.circuit.command.CircuitUpdateCommandUseCase.CircuitUpdateCommand;

import java.io.Serializable;
import java.util.UUID;

public record CircuitUpdateDto(
        UUID id,
        String name,
        String description,
        String postalCode,
        String streetName,
        String streetNumber,
        double length,
        String email,
        String phoneNumber
) implements Serializable {

    public CircuitUpdateCommand toUpdateCommand() {
        return CircuitUpdateCommand.builder()
                .withId(id)
                .withName(name)
                .withDescription(description)
                .withPostalCode(postalCode)
                .withStreetName(streetName)
                .withStreetNumber(streetNumber)
                .withLength(length)
                .withEmail(email)
                .withPhoneNumber(phoneNumber)
                .build();
    }
}