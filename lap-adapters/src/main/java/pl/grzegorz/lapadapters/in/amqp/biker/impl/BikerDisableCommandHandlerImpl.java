package pl.grzegorz.lapadapters.in.amqp.biker.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapadapters.in.amqp.biker.handler.BikerDisableCommandHandler;
import pl.grzegorz.lapadapters.in.amqp.biker.dto.BikerEnableDto;
import pl.grzegorz.lapapplication.ports.in.biker.command.BikerDisableCommandUseCase;

@Service
@RequiredArgsConstructor
class BikerDisableCommandHandlerImpl implements BikerDisableCommandHandler {

    private final BikerDisableCommandUseCase bikerDisableCommandUseCase;

    @Override
    public void disable(BikerEnableDto bikerEnableDto) {
        bikerDisableCommandUseCase.disable(bikerEnableDto.id());
    }
}