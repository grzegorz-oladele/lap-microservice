package pl.grzegorz.lapadapters.in.amqp.circuit.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapadapters.in.amqp.circuit.consumer.CircuitCreateCommandHandler;
import pl.grzegorz.lapadapters.in.amqp.circuit.dto.CircuitCreateDto;
import pl.grzegorz.lapapplication.ports.in.circuit.command.CircuitCreateCommandUseCase;

@Service
@RequiredArgsConstructor
class CircuitCreateCommandHandlerImpl implements CircuitCreateCommandHandler {

    private final CircuitCreateCommandUseCase circuitCreateCommandUseCase;

    @Override
    public void create(CircuitCreateDto circuitCreateDto) {
        circuitCreateCommandUseCase.create(circuitCreateDto.toCreateData());
    }
}