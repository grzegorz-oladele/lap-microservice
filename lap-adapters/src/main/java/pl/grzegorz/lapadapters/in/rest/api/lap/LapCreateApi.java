package pl.grzegorz.lapadapters.in.rest.api.lap;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.grzegorz.lapadapters.in.rest.dto.LapCreateDto;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

@RequestMapping("/laps")
public interface LapCreateApi {

    @PostMapping("/{circuitId}/circuits/{bikerId}/bikers/{motorcycleId}/motorcycles")
    ResponseEntity<UUID> createLap(
            @PathVariable("circuitId") String circuitId,
            @PathVariable("bikerId") String bikerId,
            @PathVariable("motorcycleId") String motorcycleId,
            @RequestBody LapCreateDto lapDto
    ) throws ExecutionException, InterruptedException;
}