package pl.grzegorz.lapadapters.in.amqp.motorcycle.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapadapters.in.amqp.motorcycle.dto.MotorcycleUnactiveDto;
import pl.grzegorz.lapadapters.in.amqp.motorcycle.handler.MotorcycleSetUnactiveCommandHandler;
import pl.grzegorz.lapapplication.ports.in.motorcycle.command.MotorcycleSetUnactiveCommandUseCase;

@Service
@RequiredArgsConstructor
class MotorcycleSetUnactiveCommandHandlerImpl implements MotorcycleSetUnactiveCommandHandler {

    private final MotorcycleSetUnactiveCommandUseCase motorcycleSetUnactiveCommandUseCase;

    @Override
    public void handle(MotorcycleUnactiveDto motorcycleUnactiveDto) {
        motorcycleSetUnactiveCommandUseCase.execute(motorcycleUnactiveDto.id().toString());
    }
}