package pl.grzegorz.lapadapters.in.amqp.biker.dto;

import pl.grzegorz.lapapplication.ports.in.biker.command.BikerUpdateCommandUseCase.BikerUpdateCommand;

import java.io.Serializable;
import java.util.UUID;

public record BikerUpdateDto(
        UUID id,
        String firstName,
        String lastName,
        String email,
        Boolean enable,
        String phoneNumber

) implements Serializable {

    public BikerUpdateCommand bikerUpdateCommand() {
        return BikerUpdateCommand.builder()
                .withId(id)
                .withFirstName(firstName)
                .withLastName(lastName)
                .withEmail(email)
                .withEnable(enable)
                .withPhoneNumber(phoneNumber)
                .build();
    }
}