package pl.grzegorz.lapadapters.in.amqp.motorcycle.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapadapters.in.amqp.motorcycle.dto.MotorcycleMapEngineDto;
import pl.grzegorz.lapadapters.in.amqp.motorcycle.handler.MotorcycleMapEngineCommandHandler;
import pl.grzegorz.lapapplication.ports.in.motorcycle.command.MotorcycleMapEngineCommandUseCase;

@Service
@RequiredArgsConstructor
class MotorcycleMapEngineCommandHandlerImpl implements MotorcycleMapEngineCommandHandler {

    private final MotorcycleMapEngineCommandUseCase motorcycleMapEngineCommandUseCase;

    @Override
    public void handle(MotorcycleMapEngineDto motorcycleMapEngineDto) {
        motorcycleMapEngineCommandUseCase.map(motorcycleMapEngineDto.toMapEngineCommand());
    }
}