package pl.grzegorz.lapadapters.in.amqp.biker.handler;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import pl.grzegorz.lapadapters.in.amqp.biker.dto.BikerDto;

public interface BikerCreateCommandHandler {

    @RabbitListener(queues = "${spring.biker.create.queue}")
    void create(BikerDto bikerDto);
}