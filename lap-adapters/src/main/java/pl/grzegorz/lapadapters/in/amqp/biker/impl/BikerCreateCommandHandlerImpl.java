package pl.grzegorz.lapadapters.in.amqp.biker.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.grzegorz.lapadapters.in.amqp.biker.dto.BikerDto;
import pl.grzegorz.lapadapters.in.amqp.biker.handler.BikerCreateCommandHandler;
import pl.grzegorz.lapapplication.ports.in.biker.command.BikerCreateCommandUseCase;

@Service
@RequiredArgsConstructor
class BikerCreateCommandHandlerImpl implements BikerCreateCommandHandler {

    private final BikerCreateCommandUseCase bikerCreateCommandUseCase;

    @Override
    public void create(BikerDto bikerDto) {
        var bikerCreateCommand = bikerDto.toCreateCommand();
        bikerCreateCommandUseCase.create(bikerCreateCommand);
    }
}