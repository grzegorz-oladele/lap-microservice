package pl.grzegorz.lapadapters.in.rest.api.lap;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.grzegorz.lapapplication.ports.view.LapView;
import pl.grzegorz.lapapplication.resources.ResourceFilter;
import pl.grzegorz.lapapplication.resources.ResultView;

@RequestMapping("/laps")
public interface LapPageApi {

    @GetMapping
    ResponseEntity<ResultView<LapView>> findAll(ResourceFilter filter);
}