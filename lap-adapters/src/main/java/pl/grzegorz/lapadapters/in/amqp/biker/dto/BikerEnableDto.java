package pl.grzegorz.lapadapters.in.amqp.biker.dto;

import java.io.Serializable;
import java.util.UUID;

public record BikerEnableDto(
        UUID id
) implements Serializable {
}
